<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LocalPostGroup extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'gmb_account_id',
        'topic_type',
        'event_title',
        'event_start_time',
        'event_end_time',
        'create_user_id',
        'update_user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates   = [
        'event_start_time',
        'event_end_time',
        'create_time',
        'update_time'
    ];

    protected $attributes = [
        'account_id' => 0,
        'gmb_account_id' => '',
        'event_title' => '',
        'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        if (Auth::user()->isAdmin()) {
            $query->where('local_post_groups.is_deleted', '=', config('const.FLG_OFF'));
        } else {
            $query->where('local_post_groups.is_deleted', '=', config('const.FLG_OFF'))
            ->whereIn('local_post_groups.account_id', Account::getMyAccounts(Auth::id())->pluck('account_id')->all());
        }
        return $query;
    }

    public function localPosts()
    {
        return $this->hasMany('App\LocalPost', 'local_post_group_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'account_id');
    }

    public function mediaItems()
    {
        return $this->hasMany('App\MediaItem', 'local_post_group_id', 'id');
    }

    /**
     * 同一の local_post_group_id を持つ local_posts のうち、最初の local_post を取得する
     */
    public static function getFirstLocalPost($local_post_group_id)
    {
        return self::findOrFail($local_post_group_id)->localPosts()->first();
    }

    public function isEditable()
    {
        $first_local_post = $this->localPosts()->get()[0];
        return ($first_local_post->sync_status == false || $first_local_post->sync_status == 'DRAFT');
    }
    public function isEditOrView()
    {
        $first_local_post = $this->localPosts()->get()[0];
        return $first_local_post;
    }
    public function getEventStartDateTimeString($default_string = '')
    {
        return $this->getEventDateTime('START', $default_string);
    }

    public function getEventEndDateTimeString($default_string = '')
    {
        return $this->getEventDateTime('END', $default_string);
    }

    private function getEventDateTime($start_or_date = 'START', $default_string = '')
    {
        $first_local_post = $this->localPosts()->first();
        $datetime_string = $default_string;
        $event_date = ($start_or_date == 'START')
            ? $first_local_post->gmb_event_start_time
            : $first_local_post->gmb_event_end_time;
        if ($first_local_post && $event_date) {
            $dt_format = $first_local_post->gmb_has_event_time == 0
                ? config('formConst.FORMAT_DATE_YMD')
                : config('formConst.FORMAT_DATETIME_YMDHI');
            $datetime_string = $event_date->format($dt_format);
        }
        return $datetime_string;
    }
}
