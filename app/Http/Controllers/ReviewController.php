<?php

namespace App\Http\Controllers;

use App\Http\Requests\Review\ExportRequest;
use App\Http\Requests\Review\PostRequest;
use App\Account;
use App\ReviewReply;
use App\Services\ReviewService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function index(Request $request, ReviewService $service)
    {
        if (is_null($request->input('account'))) {
            if (session(config('formConst.SESSION_MY_BRAND'))) {
                $account_id = session(config('formConst.SESSION_MY_BRAND'));
            } else {
                $account = Account::getMyAccounts(Auth::id())->select('account_id')->first();
                if ($account) {
                    $account_id = $account->account_id;
                } else {
                    $account_id = null;
                }
            }
            $request->merge(['account' => $account_id]);
        }
        if (is_null($request->input('period'))) {
            if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
                $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
            } else {
                $contract_type = Auth::user()->getContractType() ?? 1;
            }
            $request->merge(['period' => config('const.CONTRACT_TYPE_PERIOD')[$contract_type]]);
        }
        $accounts       = $service->findAccounts();
        $locations      = $service->findLocations($request->input('account'));
        $is_autoreplied = $service->findIsAutoreplied($request->input('account'));
        $reviews        = $service->search($request)->paginate(config('formConst.PAGINATE_MAX_LIMIT'));
        return view('review/index', compact('accounts' ,'locations', 'is_autoreplied', 'reviews'));
    }

    public function export(ExportRequest $request, ReviewService $service)
    {
        return $service->export($request);
    }

    public function change(Request $request, ReviewService $service)
    {
        try {
            $service->change($request);
            return redirect(action('ReviewController@index'))->with('success', '自動返信設定を変更しました');
        } catch (Exception $e) {
            return back()->withErrors('自動返信設定の変更時にエラーが発生しました')->withInput();
        }
    }

    public function create($reviewId, ReviewService $service)
    {
        $review    = $service->find($reviewId);
        $reply     = new ReviewReply();
        $templates = $service->findTemplates($review->location_id);
        return view('review/reply', compact('review', 'reply', 'templates'));
    }

    public function edit($reviewId, ReviewService $service)
    {
        $review    = $service->find($reviewId);
        $reply     = $service->findReply($reviewId);
        $templates = $service->findTemplates($review->location_id);
        return view('review/reply', compact('review', 'reply', 'templates'));
    }

    public function store(PostRequest $request, ReviewService $service)
    {
        try {
            $service->store($request);
            return redirect(action('ReviewController@index'))->with('success', '返信データを保存しました');
        } catch (Exception $e) {
            return back()->withErrors('返信データの保存時にエラーが発生しました')->withInput();
        }
    }

    public function update(PostRequest $request, ReviewService $service)
    {
        try {
            $service->update($request);
            return redirect(action('ReviewController@index'))->with('success', '返信データを変更しました');
        } catch (Exception $e) {
            return back()->withErrors('返信データの変更時にエラーが発生しました')->withInput();
        }
    }

    public function delete(PostRequest $request, ReviewService $service)
    {
        try {
            $service->delete($request);
            return redirect(action('ReviewController@index'))->with('success', '返信データを削除しました');
        } catch (Exception $e) {
            return back()->withErrors('返信データの削除時にエラーが発生しました');
        }
    }

    public function asyncFindIsAutoreplied(Request $request, ReviewService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->findIsAutoreplied($request->input('account'))];
        }
    }

    public function asyncFindLocations(Request $request, ReviewService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->findLocations($request->input('account'))];
        }
    }

    public function asyncUpdateAutoReplied(Request $request, ReviewService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->updateAutoreplied($request)];
        }
    }
    
    public function asyncGetAutoReplied(Request $request, ReviewService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->findIsAutoreplied($request->input('account'))];
        }
    }
}
