<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MediaItem extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'media_item_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'local_post_id',
        'local_post_group_id',
        'gmb_account_id',
        'gmb_location_id',
        'gmb_media_key',
        'gmb_media_format',
        'gmb_location_association_category',
        'gmb_location_association_price_list_item_id',
        'gmb_google_url',
        'gmb_thumbnail_url',
        'gmb_create_time',
        'gmb_dimentions_width_pixels',
        'gmb_dimentions_height_pixels',
        'gmb_insights_view_count',
        'gmb_attribution_profile_name',
        'gmb_attribution_profile_photo_url',
        'gmb_attribution_takedown_url',
        'gmb_atttribution_profile_url',
        'gmb_description',
        'gmb_source_url',
        'gmb_data_ref_resource_name',
        's3_object_url',
        'is_deleted',
        'sync_type',
        'sync_status',
        'scheduled_sync_time',
        'sync_time',
        'create_user_id',
        'update_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = ['create_time', 'update_time'];

    protected $attributes = [
        'local_post_id' => 0,
        'gmb_account_id' => '',
        'gmb_location_id' => '',
        'gmb_media_key' => '',
        'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        return $query->where('is_deleted', '=', config('const.FLG_OFF'));
    }

    public function localPost()
    {
        return $this->belongsTo('App\LocalPost', 'local_post_id', 'local_post_id');
    }

    public function getImageUrl()
    {
        $image_url = asset('img/no_image.png');
        if ($this->gmb_thumbnail_url) {
            $image_url = $this->gmb_thumbnail_url;
        } elseif ($this->s3_object_url) {
            $image_url = Storage::url($this->s3_object_url);
        } elseif ($this->gmb_google_url) {
            $image_url = $this->gmb_google_url;
        }

        return $image_url;
    }

    public function getThumbnailImageUrl()
    {
        $image_url = $this->getImageUrl();

        //拡張子の前に文字列を追加
        $pos = strrpos($image_url, '.'); // .が最後に現れる位置
        if ($pos && $image_url !== asset('img/no_image.png')) {
            $filePath = substr($image_url, 0, $pos) . config('const.THUMBNAIL_PREFIX') . substr($image_url, $pos);

            // S3に配置されている相対パスを取得する
            $thumPos = strrpos($this->s3_object_url, '.');
            $thumFilePath = substr($this->s3_object_url, 0, $thumPos) . config('const.THUMBNAIL_PREFIX') . substr($this->s3_object_url, $thumPos);

            // 存在すればファイルパスを更新
            if(Storage::exists($thumFilePath)) {
                $image_url = $filePath;
            }
        }

        return $image_url;
    }
}
