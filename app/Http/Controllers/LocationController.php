<?php

namespace App\Http\Controllers;

use App\Account;
use App\Attribute;
use App\Category;
use App\Http\Requests\Location\LocationRequest;
use App\Location;
use App\Services\LocationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, LocationService $service)
    {
        // TODO: 機能未実装のため一時的にコメントアウト
//        $params = [
//            'locations' => $service->search($request)->paginate(config('formConst.PAGINATE_MAX_LIMIT')),
//            'accounts' => Account::active()->pluck('gmb_account_name', 'account_id')->all(),
//            'attributes' => Attribute::whereIn('location_id', Location::getMyLocations(Auth::id()))->pluck('gmb_attributes_values', 'attribute_id'),
//        ];
//        return view('location/index', $params);

        return view('location/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'isCreate' => true,
            'location' => new Location(),
            'categories' => Category::active()->get(),
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('location/create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request, LocationService $service)
    {
        try {
            $service->store($request);
            return redirect(action("LocationController@edit", ['locationId' => $request->input('location_id')]))
                ->with('success', '店舗を保存しました');
        } catch (Exception $e) {
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($location_id, LocationService $service)
    {
        $params = [
            'location' => $service->find($location_id),
            'isCreate' => false,
            'categories' => Category::active()->get(),
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('location/create', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, LocationService $service)
    {
        try {
            $service->update($request);
            return redirect(action("LocationController@edit", ['locationId' => $request->input('location_id')]))
                ->with('success', '店舗を保存しました');
        } catch (Exception $e) {
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, LocationService $service)
    {
        try {
            $service->delete($id);
            return redirect(action("LocationController@index"))
                ->with('success', '店舗を削除しました');
        } catch (Exception $e) {
            return back()->withErrors('データの削除時にエラーが発生しました');
        }
    }
}
