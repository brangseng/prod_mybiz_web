<?php

namespace App\Services;

use App\LocalPost;
use App\Location;
use App\MediaItem;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LocationService
{
    public function find($locationId)
    {
        return Location::findOrFail($locationId);
    }

    public function search(Request $request)
    {
        $location = $this->getCondition($request)->orderBy('update_time', 'desc');
        return $location;
    }

    public function store(Request $request)
    {
        try {

            // [ToDo] : ログインユーザーに紐づく account_id の設定が必要。
            // 現状、Location.php モデルの protected $attributes で 1 固定で保存。
            
            DB::beginTransaction();
            $this->setSyncStatus($request, true); // sync_status をセットする
            $location = Location::create($this->getFilteredRequest($request));
            // 新規登録された location の location_id を セットしておく
            // （登録完了後の画面 ＝ edit 画面を呼び出す際に必要）
            $request->merge(['location_id' => $location->location_id]);
            // 追加カテゴリー
            $location->categories()->sync($request->gmb_sub_category_ids);

            DB::commit();
            return $location;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            // report($e);
            throw $e;
        }
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->setSyncStatus($request, false); // sync_status をセットする
            $filtered_request = $this->getFilteredRequest($request);
            $location = Location::findOrFail($request->input('location_id'));
            $location->fill($filtered_request)->save();
            // 追加カテゴリー
            $location->categories()->sync($request->gmb_sub_category_ids);
            DB::commit();
            return $location;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            // report($e);
            throw $e;
        }
    }

    private function setSyncStatus($request, $is_create)
    {
        $request->merge(['sync_type' => $is_create ? 'CREATE' : 'PATCH']);
        $request->merge(['sync_status' => 'QUEUED']);
        $request->merge(['scheduled_sync_time' => null]);
    }

    private function getFilteredRequest($request)
    {
        $filtered_array = array_filter($request->all(), function ($value, $key) {
            // 配列・Object は削除。
            $isArray = is_array($value);
            $isObject = is_object($value);
            return !$isArray && !$isObject;
        }, ARRAY_FILTER_USE_BOTH);
        return $filtered_array;
    }

    public function getCondition(Request $request)
    {
        // $start_date = $request->input('stDate') ? Carbon::parse($request->input('stDate'))->toIso8601ZuluString() : null;
        // $end_date = $request->input('endDate') ? Carbon::parse($request->input('endDate'))->toIso8601ZuluString() : null;
        $account_id = $request->input('account_id');
        $gmb_primary_category_id = $request->input('gmb_primary_category_id');
        $attribute_id = $request->input('attribute_id');
        $keyword = $request->input('keyword');
        
        $location = Location::active();
        if($location->count() <= 0)
        {
            return $location;
        }
        /*
        if ($start_date) {
            $location = $location->where('sync_time', '>=', $start_date);
        }
        if ($end_date) {
            $location = $location->where('sync_time', '<=', $end_date);
        }
        */
        if ($account_id) {
            $location = $location->where('account_id', '=', $account_id);
        }
        if ($gmb_primary_category_id) {
            $location = $location->where('gmb_primary_category_id', '=', $gmb_primary_category_id);
        }
        if ($attribute_id) {
            $location = $location->whereHas('attributes', function ($query) use ($attribute_id) {
                $query->where('location_attributes.attribute_id', '=', $attribute_id);
            });
        }
        if ($keyword) {
            $location = $location->where('gmb_location_name', 'like', "%${keyword}%")
                ->orWhere('gmb_profile_description', 'like', "%${keyword}%");
        }
        if (0 < $location->first()->localPosts()->count()) {
            DB::enableQueryLog();
            $l = $location->first()->localPosts()->oldest()->first()->sync_time;
            Log::debug(DB::getQueryLog());
        }
        // $location = $location->with(['localPosts' => function ($query) {
        //     $query->max('local_posts.update_time');
        // }]);
        return $location;
    }

    public function export(Request $request)
    {
        try {
            $location = $this->getCondition($request);
            $location = $location->select(config('const.CSV_ITEMS.LOCAL_POST'))->get();
            if (!empty($location)) {
                $location = $location->toArray();
            } else {
                $location = [];
            }
            return outPutCsv(
                $location,
                config('const.CSV_HEADERS.LOCAL_POST'),
                config('const.EXPORT_LOCAL_POSTS_FILE_NAME') . '_' . date('ymd')
            );
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function delete($location_id)
    {
        try{
            $location = Location::find($location_id);
            $location->localPosts()->detach();
            $location->accounts()->detach();
            $location->attributes()->detach();
            $location->categories()->detach();
            $location->delete();
            
        } catch(Exception $e) {
            logger()->error($e->getMessage());
            throw $e;
        }
    }
}
