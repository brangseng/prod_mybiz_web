<?php

namespace App\Http\Controllers;

use App\Account;
use App\Services\HomeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, HomeService $service)
    {
        
        if (is_null($request->input('account'))) {
            if (session(config('formConst.SESSION_MY_BRAND'))) {
                $account_id = session(config('formConst.SESSION_MY_BRAND'));
            } else {
                $account = Account::getMyAccounts(Auth::id())->select('account_id')->first();
                if ($account) {
                    $account_id = $account->account_id;
                } else {
                    $account_id = null;
                }
            }
            $request->merge(['account' => $account_id]);
        }
        if (is_null($request->input('dateRange'))) {
            $request->merge(['dateRange' => config('const.DASHBOARD_DATE_RANGE_DEFAULT')]);
        }
        if (is_null($request->input('period'))) {
            if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
                $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
            } else {
                $contract_type = Auth::user()->getContractType() ?? 1;
            }
            $request->merge(['period' => config('const.CONTRACT_TYPE_PERIOD')[$contract_type]]);
        }
        if (is_null($request->input('chartType'))) {
            $request->merge(['chartType' => config('const.DASHBOARD_CHART_TYPE_DEFAULT')]);
        }
        $accounts  = $service->findAccounts();
        $tiles     = $service->getTiles($request);
        $lists     = $service->getLists($request);
        return view('home', compact('accounts', 'tiles', 'lists'));
    }

    public function export(Request $request, HomeService $service)
    {
        return $service->export($request);
    }

    public function asyncGetDateRanges(Request $request, HomeService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->getSelectDateRanges($request)];
        }
    }

    public function asyncGetTiles(Request $request, HomeService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->getTiles($request)];
        }
    }

    public function asyncGetCharts(Request $request, HomeService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->getCharts($request)];
        }
    }

    public function asyncGetLists(Request $request, HomeService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->getLists($request)];
        }
    }
}
