<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\LocalPost\LocalPostRequest;
use App\LocalPost;
use App\LocalPostGroup;
use App\Location;
use App\Services\LocalPostGroupService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class LocalPostGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, LocalPostGroupService $service)
    {

        $localPostGroups = $service->search($request)->paginate(config('formConst.PAGINATE_MAX_LIMIT'));
        // $localPostGroups = $service->search($request)->get();
        $params = [
            'stDate' => $request->stDate,
            'endDate' => $request->endDate,
            'gmb_topic_type' => $request->gmb_topic_type,
            'gmb_action_type' => $request->gmb_action_type,
            'sync_status' => $request->sync_status,
            'accounts' => $service->findAccounts(),
        ];

        return view('localpost/index', compact('localPostGroups', 'params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $variables = [
            'local_post_group' => new LocalPostGroup(),
            'localpost' => new LocalPost(),
            'isCreate' => true,
            'isReadOnly' => false,
            'locations' => Location::getMyLocations(Auth::id())->get(),
            'accounts' => Account::getMyAccounts(Auth::id())->get(),
            'prefs' => config('pref'),
        ];
        return view('localpost/create', $variables);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocalPostRequest $request, LocalPostGroupService $service)
    {
        try {
            $localpost_group = $service->store($request);
            return redirect(action(
                "LocalPostGroupController@edit",
                ['localPostGroupId' => $localpost_group->id]
            ))->with('success', '投稿を保存しました');
        } catch (Exception $e) {
            report($e);
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocalPost  $localPostGroup
     * @return \Illuminate\Http\Response
     */
    public function show(LocalPostGroup $localPostGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocalPost  $localPostGroup
     * @return \Illuminate\Http\Response
     */
    public function edit($localPostGroupId, LocalPostGroupService $service)
    {
        if (Gate::denies('show-localpost', LocalPostGroup::findOrFail($localPostGroupId))) {
            abort(403, '権限がありません');
        }

        // View で編集するのは local_posts のレコード。
        $first_local_post = LocalPostGroup::getFirstLocalPost($localPostGroupId);

        if (!$first_local_post) {
            return App::abort(404);
        }

        $params = [
            'local_post_group' => LocalPostGroup::findOrFail($localPostGroupId),
            'localpost' => $first_local_post,
            'isCreate' => false,
            'isReadOnly' => !$first_local_post->isEditable(),
            'locations' => Location::getMyLocations(Auth::id())->get(),
            'accounts' => Account::getMyAccounts(Auth::id())->get(),
            'prefs' => config('pref'),
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('localpost/create', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocalPost  $localPost
     * @return \Illuminate\Http\Response
     */
    public function update(LocalPostRequest $request, LocalPostGroupService $service)
    {
        if (Gate::denies('show-localpost', LocalPostGroup::findOrFail($request->local_post_group_id))) {
            abort(403, '権限がありません');
        }

        try {
            $service->update($request);
            return redirect(action(
                "LocalPostGroupController@edit",
                ['localPostGroupId' => $request->input('local_post_group_id')]
            ))->with('success', '投稿を保存しました');
        } catch (Exception $e) {
            logger()->error($e);
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocalPostGroup  $localPostGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($localPostGroupId, LocalPostGroupService $service)
    {
        if (Gate::denies('show-localpost', LocalPostGroup::findOrFail($localPostGroupId))) {
            abort(403, '権限がありません');
        }

        try {
            $service->destroy($localPostGroupId);
            return redirect(action("LocalPostGroupController@index"))
                ->with('success', '投稿を削除しました');
        } catch (Exception $e) {
            logger()->error($e);
            return back()->withErrors('データの削除時にエラーが発生しました')->withInput();
        }
    }

    public function export(Request $request, LocalPostGroupService $service)
    {
        try {
            return $service->export($request);
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function upload(Request $request)
    {
        $upload_path = 'gmb/media'
            . '/' . (mb_strtolower($request->gmb_topic_type ?? 'other'))
            . '/' . ($request->account_id ?? "no_account_id")
            . '/' . ($request->id ?? "no_local_post_group_id");
        $filename = Carbon::now()->format('YmdHis') . '_' . $this->getRandomStr(5);
        $extension = $request->file('file')->extension();
        $filepath = $request->file('file')->storeAs(
            $upload_path,
            $filename . '.' . $extension,
            ['ACL' => 'public-read']
        );
        return response()->json($filepath);
    }

    private function getRandomStr($length)
    {
        return substr(bin2hex(random_bytes($length)), 0, $length);
    }
}
