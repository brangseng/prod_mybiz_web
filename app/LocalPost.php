<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalPost extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'local_post_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_id',
        'local_post_group_id',
        'gmb_account_id',
        'gmb_location_id',
        'gmb_local_post_id',
        'gmb_language_code',
        'gmb_summary',
        'gmb_action_type',
        'gmb_action_type_url',
        'gmb_create_time',
        'gmb_update_time',
        'gmb_event_title',
        'gmb_has_event_time',
        'gmb_event_start_time',
        'gmb_event_end_time',
        'gmb_local_post_state',
        'gmb_search_url',
        'gmb_topic_type',
        'gmb_offer_coupon_code',
        'gmb_offer_redeem_online_url',
        'gmb_offer_terms_conditions',
        'is_deleted',
        'sync_type',
        'sync_status',
        'scheduled_sync_time',
        'sync_time',
        'create_user_id',
        'update_user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates   = [
        'gmb_event_start_time',
        'gmb_event_end_time',
        'scheduled_sync_time',
        'sync_time',
        'create_time',
        'update_time'
    ];

    protected $attributes = [
        'location_id' => 0,
        'gmb_account_id' => '',
        'gmb_location_id' => '',
        'gmb_local_post_id' => '',
        'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        return $query->where('is_deleted', '=', config('const.FLG_OFF'));
    }

    public function mediaItems()
    {
        return $this->hasMany('App\MediaItem', 'local_post_id', 'local_post_id');
    }

    public function locations()
    {
        return $this->belongsTo('App\Location', 'location_id', 'location_id');
    }

    public function localPostGroup()
    {
        return $this->belongsTo(LocalPostGroup::class);
    }

    public function isEditable()
    {
        // LocalPostGroup の メソッドを利用する
        return $this->localPostGroup->isEditable();
    }
}
