<?php

/*
 * custom helpers
 */

use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;

if (!function_exists('mb_str_pad')) {
    function mb_str_pad($input, $pad_string, $pad_length, $replace_pad_string = " ", $encoding = "UTF-8")
    {
        $before = str_repeat($pad_string, $input);
        $mb_length = mb_strlen($before, $encoding);
        return $before . str_repeat($replace_pad_string, $pad_length - $mb_length);
    }
}

if (!function_exists('outPutCsv')) {
    function outPutCsv(array $lists, array $header = [], $fileName = '')
    {
        $response = new \Symfony\Component\HttpFoundation\StreamedResponse(function () use ($lists, $header) {
            $stream = fopen('php://output', 'w');
            stream_filter_prepend($stream, 'convert.iconv.utf-8/utf-8');
            fwrite($stream, "\xEF\xBB\xBF");
            if (!empty($header)) {
                fputcsv($stream, $header);
            }
            foreach ($lists as $list) {
                fputcsv($stream, $list);
            };
            fclose($stream);
        });
        $fileName = (empty($fileName)) ? date('ymd') : $fileName;
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $fileName . '.csv');
        return $response;
    }
}

if (!function_exists('saveMyBrandToSessionCookie')) {
    function saveMyBrandToSessionCookie($account_id)
    {
        // 選択されたブランドをSession/Cookieに保持しておく
        session([config('formConst.SESSION_MY_BRAND') => $account_id]);
        Cookie::queue(
            config('formConst.SESSION_MY_BRAND'),
            $account_id,
            config('formConst.SESSION_MY_BRAND_TTL'),
            '/',    // path
            null,   // domain
            config('app.env') == 'local' ? false : true,   // secure
            config('app.env') == 'local' ? false : true    // httponly
        );
    }
}

if (!function_exists('getNextSyncDateTime')) {
    function getNextSyncDateTime(Carbon $target_datetime = null, $function = null)
    {
        // 過去日時が指定された場合はAPI連携されないので null を返す
        if ($target_datetime && $target_datetime->isPast()) {
            return null;
        }

        $result_datetime = null;

        if (!$target_datetime) {
            $target_datetime = new Carbon();
        }
        if ($function && $function == 'REPLY') {
            $time_ranges = config('formConst.REPLY_TIME_RANGE');
        } else {
            $time_ranges = config('formConst.POST_TIME_RANGE');
        }
        if (!array_key_exists('24:00', $time_ranges)) {
            $time_ranges = array_merge($time_ranges, ['24:00' => '－']);
        }

        // 日付をまたぐ場合を考慮し、時間帯として当日分($addDay=0)と翌日分($addDay=1)までループさせれば
        // いずれかの時間帯にマッチするはず
        for ($addDay = 0; $addDay <= 1; $addDay++) {
            // formConst.POST_TIME_RANGE のキーが時刻表記 H:i になっているので、このキーを基準にする
            $last_key = '00:00';
            foreach ($time_ranges as $key => $range) {
                $start = Carbon::parse($target_datetime->format('Y-m-d ') . $last_key)->addDays($addDay);
                $end = Carbon::parse($target_datetime->format('Y-m-d ') . $key)->addDays($addDay);
                if ($target_datetime->gte($start) && $target_datetime->lt($end)) {
                    $result_datetime = $end;
                    break;
                }
                $last_key = $key;
            }

            if ($result_datetime) {
                break;
            }
        }

        // 次回のAPI連携が判定できない場合は null を返す
        return $result_datetime;
    }
}
