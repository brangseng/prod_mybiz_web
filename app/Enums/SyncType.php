<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static CREATE()
 * @method static static PATCH()
 * @method static static DELETE()
 */
final class SyncType extends Enum
{
    const CREATE = 1;
    const PATCH = 2;
    const DELETE = 3;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value):string
    {
        switch ($value){
            case self::CREATE:
                return 'CREATE';
                brake;
            case self::PATCH:
                return 'PATCH';
                brake;
            case self::DELETE:
                return 'DELETE';
                brake;
            default:
                return self::getKey($value);
        }
    }

    public static function getValue(string $key)
    {
        switch ($key){
            case 'CREATE':
                return 1;
            case 'PATCH':
                return 2;
            case 'DELETE':
                return 3;
            default:
                return self::getValue($key);
        }
    }

}
