<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'enterprise_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise', 'enterprise_id', 'enterprise_id');
    }

    public function roles()
    {
        return $this->hasMany(Role::class, 'role_id', 'role_id');
    }

    public function isAdmin()
    {
        // return $this->roles()->where('role_id', Role::ADMIN_ROLE_ID)->exists();
        // システム全体の管理権限なので、users.id = 1 に固定する。
        return ($this->user_id == 1);
    }

    /**
     * ログインユーザーが所属している企業の「契約タイプ」を取得する。
     * 通常は session を使用し、取得できなければDBから参照。
     *
     * @return integer
     */
    public function getContractType() : ?int
    {
        $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE')) ?? $this->enterprise->contract_type;
        return $contract_type;
    }

    public function setContractType($contract_type) : void
    {
        session([config('formConst.SESSION_CONTRACT_TYPE') => $contract_type]);
    }
}
