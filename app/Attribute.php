<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'attribute_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_id',
        'gmb_attributes_attribute_id',
        'gmb_attributes_value_type',
        'gmb_attributes_values',
        'gmb_attributes_repeated_set_values',
        'gmb_attributes_repeated_unset_values',
        'gmb_attributes_url_values',       
    ];

    protected $dates   = ['create_time', 'update_time'];

    protected $attributes = [
        // 'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        // return $query->where('is_deleted', '=', config('const.FLG_OFF'));
        return $query;
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'location_attributes', 'attribute_id', 'location_id');
    }
}
