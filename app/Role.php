<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN_ROLE_ID = 1;
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'role_id';

    protected $fillable = [
        'name',
        'explanation',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'user_id', 'user_id');
    }
}
