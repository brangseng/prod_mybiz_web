<?php

namespace App;

use App\Enums\SyncStatusType;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'review_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sync_type',
        'sync_status',
        'scheduled_sync_time',
        'update_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [
        'gmb_create_time',
        'gmb_update_time',
        'scheduled_sync_time',
        'create_time',
        'update_time'
    ];

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'location_id');
    }

    public function reply()
    {
        return $this->hasOne(ReviewReply::class, 'review_reply_id', 'review_reply_id');
    }

    public function scopeActive($query)
    {
        return $query->where('reviews.is_deleted', '=', config('const.FLG_OFF'));
    }

    public function scopeActiveReplies($query)
    {
        return $query->where('review_replies.is_deleted', '=', config('const.FLG_OFF'));
    }

    public function scopeByLocations($query, $locations)
    {
        return $query->whereIn('reviews.location_id', $locations);
    }

    public function scopeByCreateStDate($query, $stDate)
    {
        return $query->where('reviews.gmb_create_time', '>=', $stDate);
    }

    public function scopeByCreateEndDate($query, $endDate)
    {
        return $query->where('reviews.gmb_create_time', '<=', $endDate);
    }

    public function scopeBetweenCreateDate($query, $stDate, $endDate)
    {
        return $query->whereBetween('reviews.gmb_create_time', [$stDate, $endDate]);
    }

    public function scopeByStarRating($query, $rate)
    {
        return $query->where('reviews.gmb_star_rating', '=', $rate);
    }

    public function scopeReviewReplied($query)
    {
        return  $query->where('reviews.gmb_review_reply_comment', '!=', null)
            ->where('reviews.gmb_review_reply_comment', '!=', '');
    }

    public function scopeReviewUnreplied($query)
    {
        return $query->where(function($query) {
            $query->orWhere('reviews.gmb_review_reply_comment', '=', null)
                ->orWhere('reviews.gmb_review_reply_comment', '=', '');
        });
    }

    public function scopeBySyncStatus($query, $syncStatus)
    {
        return $query->where(function($query) use ($syncStatus) {
            $query->orWhere('reviews.sync_status', '=', $syncStatus)
                ->orWhere('review_replies.sync_status', '=', $syncStatus);
        });
    }

    public function scopeLeftJoinReviewReplies($query)
    {
        return $query->leftJoin('review_replies', function ($join) {
            $join->on('reviews.review_id', '=', 'review_replies.review_id')
                ->where('review_replies.is_deleted', '=', config('const.FLG_OFF'));
        });
    }

    public function scopeLeftJoinLocations($query)
    {
        return $query->leftJoin('locations', function ($join) {
            $join->on('reviews.location_id', '=', 'locations.location_id')
                ->where('locations.is_deleted', '=', config('const.FLG_OFF'));
        });
    }
}
