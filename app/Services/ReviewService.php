<?php

namespace App\Services;

use App\Enums\StarRatingType;
use App\Enums\SyncStatusType;
use App\Enums\SyncType;
use App\Account;
use App\Location;
use App\Review;
use App\ReviewReply;
use App\ReviewReplyTemplate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReviewService
{
    public function find($reviewId)
    {
        return Review::findOrFail($reviewId);
    }

    public function search(Request $request)
    {
        $reviews = $this->getCondition($request)
            ->select(config('const.SEARCH_RESULT_ITEMS.REVIEW'));

        if ($request->filled('account')) {
            saveMyBrandToSessionCookie($request->account);
        }

        return $reviews;
    }

    public function export(Request $request)
    {
        $reviews = $this->getCondition($request)
            ->select(config('const.CSV_ITEMS.REVIEW'))
            ->addSelect([
                DB::raw(
                    '(case reviews.gmb_star_rating
                          when "STAR_RATING_UNSPECIFIED" then "☆☆☆☆☆"
                          when "ONE" then "★☆☆☆☆"
                          when "TWO" then "★★☆☆☆"
                          when "THREE" then "★★★☆☆"
                          when "FOUR" then "★★★★☆"
                          when "FIVE" then "★★★★★"
                          else "☆☆☆☆☆"
                      end) as gmb_star_rating')
            ])
            ->get();
        if (!empty($reviews)) {
            $reviews = $reviews->toArray();
        } else {
            $reviews = [];
        }

        return outPutCsv($reviews, config('const.CSV_HEADERS.REVIEW'), config('const.EXPORT_REVIEWS_FILE_NAME') . '_' . date('ymd'));
    }

    public function findAccounts()
    {
        $accounts = $this->getAccounts()
            ->select('account_id', 'gmb_account_name')
            ->pluck('gmb_account_name', 'account_id')
            ->all();

        return $accounts;
    }

    public function findLocations($accountId)
    {
        $locations = $this->getLocations();
        if ($accountId) {
            $locations = $locations
                ->where('account_id', $accountId);
        }
        $locations = $locations
            ->select('location_id', 'gmb_location_name')
          //  ->pluck('gmb_location_name', 'location_id')
            ->pluck('location_id', 'gmb_location_name')
            ->all();

        return $locations;
    }

    public function findIsAutoreplied($accountId)
    {
        $autoreplied = $this->getLocations()
            ->where('account_id', $accountId)
            ->select('review_is_autoreplied')
            ->first();

        if ($autoreplied) {
            $is_autoreplied = $autoreplied->review_is_autoreplied;
        } else {
            $is_autoreplied = null;
        }

        return $is_autoreplied;
    }

    public function findReply($reviewId)
    {
        $reply = ReviewReply::active()
            ->previousReply($reviewId)
            ->select(config('const.SEARCH_RESULT_ITEMS.REPLY'))
            ->first();

        return $reply;
    }

    public function findTemplates($locationId)
    {
        $accounts = $this->getLocations()
            ->where('location_id', $locationId)
            ->select('account_id')
            ->pluck('account_id')
            ->all();

        $templates = ReviewReplyTemplate::active()
            ->whereIn('account_id', $accounts)
            ->manualReply()
            ->select('review_reply_template_id', 'template_name')
            ->pluck('template_name', 'review_reply_template_id')
            ->all();

        return $templates;
    }

    public function change(Request $request)
    {
        DB::beginTransaction();
        try {
            $update_column = [
                'review_is_autoreplied' => $request->input('isAutoreplied'),
                'update_user_id' => Auth::id()
            ];
            Location::where('account_id', $request->input('account'))
                ->update($update_column);
            DB::commit();
            if ($request->filled('account')) {
                saveMyBrandToSessionCookie($request->account);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->setSyncStatus($request);
            ReviewReply::create($request->all());
            Review::findOrFail($request->input('review_id'))
                ->fill([
                    'sync_type' => $request->input('sync_type'),
                    'sync_status' => $request->input('sync_status'),
                    'scheduled_sync_time' => $request->input('scheduled_sync_time'),
                    'update_user_id' => Auth::id()
                ])
                ->save();
            DB::commit();
            if ($request->filled('account_id')) {
                saveMyBrandToSessionCookie($request->account_id);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->setSyncStatus($request);
            ReviewReply::findOrFail($request->input('review_reply_id'))
                ->fill($request->all())
                ->save();
            Review::findOrFail($request->input('review_id'))
                ->fill([
                    'sync_type' => $request->input('sync_type'),
                    'sync_status' => $request->input('sync_status'),
                    'scheduled_sync_time' => $request->input('scheduled_sync_time'),
                    'update_user_id' => Auth::id()
                ])
                ->save();
            DB::commit();
            if ($request->filled('account_id')) {
                saveMyBrandToSessionCookie($request->account_id);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->setSyncStatusDelete($request);
            ReviewReply::findOrFail($request->input('review_reply_id'))
                ->fill($request->all())
                ->save();
            Review::findOrFail($request->input('review_id'))
                ->fill([
                    'sync_type' => $request->input('sync_type'),
                    'sync_status' => $request->input('sync_status'),
                    'scheduled_sync_time' => $request->input('scheduled_sync_time'),
                    'update_user_id' => Auth::id()
                ])
                ->save();
            DB::commit();
            if ($request->filled('account_id')) {
                saveMyBrandToSessionCookie($request->account_id);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    private function getAccounts()
    {
        $accounts = Account::getMyAccounts(Auth::id());

        return $accounts;
    }

    private function getLocations()
    {
        $locations = Location::getMyLocations(Auth::id());

        return $locations;
    }

    private function getCondition(Request $request)
    {
        $dateRanges = $this->getDateRanges($request);

        $locations = $this->getLocations();
        if ($request->filled('account')) {
            $locations = $locations
                ->where('account_id', $request->input('account'));
        }
        if ($request->filled('location')) {
            $locations = $locations
                ->where('location_id', $request->input('location'));
        }
        $locations = $locations
            ->select('location_id')
            ->pluck('location_id')
            ->all();

        $reviews = Review::active()
            ->leftJoinReviewReplies()
            ->leftJoinLocations()
            ->byLocations($locations);

        if ($dateRanges['stDate'] && $dateRanges['endDate']) {
            $reviews = $reviews
                ->betweenCreateDate($dateRanges['stDate'], $dateRanges['endDate'] . ' 23:59:59');
        } elseif ($dateRanges['stDate']) {
            $reviews = $reviews
                ->byCreateStDate($dateRanges['stDate']);
        } elseif ($dateRanges['endDate']) {
            $reviews = $reviews
                ->byCreateEndDate($dateRanges['endDate'] . ' 23:59:59');
        }

        if ($request->filled('rate')) {
            $reviews = $reviews
                ->byStarRating(StarRatingType::getDescription($request->input('rate')));
        }

        if ($request->filled('replyStatus')) {
            switch ($request->input('replyStatus')) {
                case config('const.FLG_OFF'):
                    $reviews = $reviews
                        ->reviewUnreplied();
                    break;
                case config('const.FLG_ON'):
                    $reviews = $reviews
                        ->reviewReplied();
                    break;
            }
        }

        if ($request->filled('syncStatus')) {
            $reviews = $reviews
                ->bySyncStatus(SyncStatusType::getDescription($request->input('syncStatus')));
        }

        $reviews = $reviews
            ->orderBy('reviews.gmb_create_time', 'desc');

        return $reviews;
    }

    private function getDateRanges(Request $request)
    {
        if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
            $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
        } else {
            $contract_type = Auth::user()->getContractType() ?? 1;
        }
        $contract_type_period = config('const.CONTRACT_TYPE_PERIOD')[$contract_type];
        $periodStartDate = Carbon::parse(Carbon::now()->subMonths($contract_type_period));
        $periodEndDate = Carbon::parse(Carbon::today());
        if ($contract_type_period == 0) {
            if ($request->filled('stDate')) {
                $stDate = $request->input('stDate');
            } else {
                $stDate = null;
            }
            if ($request->filled('endDate')) {
                $endDate = $request->input('endDate');
            } else {
                $endDate = null;
            }
        } else {
            $inputStartDate = Carbon::parse($request->input('stDate'));
            if ($request->filled('stDate') && $inputStartDate->gte($periodStartDate)) {
                $stDate = $request->input('stDate');
            } else {
                $stDate = $periodStartDate->format('Y-m-d');
            }
            $inputEndDate = Carbon::parse($request->input('endDate'));
            if ($request->filled('endDate') && $inputEndDate->lte($periodEndDate)) {
                $endDate = $request->input('endDate');
            } else {
                $endDate = $periodEndDate->format('Y-m-d');
            }
        }

        $dateRanges = array(
            'stDate'  => $stDate,
            'endDate' => $endDate
        );

        return $dateRanges;
    }

    private function setSyncStatus(Request $request)
    {
        $sync_type = null;
        $sync_status = null;
        $scheduled_sync_time = null;
        if (empty($request->input('sync_type'))) {
            $sync_type = SyncType::getDescription(1);
        } else if ($request->input('sync_type') == SyncType::getDescription(3)) {
            if ($request->input('sync_status') == SyncStatusType::getDescription(5)) {
                $sync_type = SyncType::getDescription(1);
            } else {
                if ($request->input('gmb_review_reply_comment')) {
                    $sync_type = SyncType::getDescription(2);
                } else {
                    $sync_type = SyncType::getDescription(1);
                }
            }
        } else {
            if ($request->input('sync_status') == SyncStatusType::getDescription(5) && $request->input('gmb_review_reply_comment')) {
                $sync_type = SyncType::getDescription(2);
            } else {
                $sync_type = $request->input('sync_type');
            }
        }
        if ($request->input('is_scheduled')) {
            $sync_status = SyncStatusType::getDescription(2);
            $scheduled_sync_time = Carbon::parse($request->input('scheduled_sync_time') . ' ' . $request->input('scheduled_range'))->format('Y-m-d H:i:s');
        } else {
            $sync_status = SyncStatusType::getDescription(1);
        }

        $request->merge(['sync_type' => $sync_type]);
        $request->merge(['sync_status' => $sync_status]);
        $request->merge(['scheduled_sync_time' => $scheduled_sync_time]);
    }

    private function setSyncStatusDelete(Request $request)
    {
        $sync_type = null;
        $sync_status = null;
        $scheduled_sync_time = null;
        if ($request->input('is_scheduled')) {
            $sync_type = SyncType::getDescription(3);
            $sync_status = SyncStatusType::getDescription(2);
            $scheduled_sync_time = Carbon::parse($request->input('scheduled_sync_time') . ' ' . $request->input('scheduled_range'))->format('Y-m-d H:i:s');
        } else {
            $sync_type = SyncType::getDescription(3);
            if ($request->input('sync_status') == SyncStatusType::getDescription(1)) {
                $sync_status = SyncStatusType::getDescription(1);
            } else {
                $sync_status = SyncStatusType::getDescription(4);
            }
        }

        $request->merge(['is_deleted' => $request->input('is_deleted')]);
        $request->merge(['sync_type' => $sync_type]);
        $request->merge(['sync_status' => $sync_status]);
        $request->merge(['scheduled_sync_time' => $scheduled_sync_time]);
    }

    public function updateAutoreplied(Request $request)
    {
        DB::beginTransaction();
        try {
            $update_column = [
                'review_is_autoreplied' => $request->input('autoReplied'),
                'update_user_id' => Auth::id()
            ];
            Location::where('account_id', $request->input('account'))
                ->update($update_column);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }
}
