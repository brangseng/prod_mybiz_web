<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialHour extends Model
{
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
}
