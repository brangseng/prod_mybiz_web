<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gmb_category_id',
        'gmb_display_name',
    ];

    protected $dates   = ['create_time', 'update_time'];

    protected $attributes = [
        // 'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        // return $query->where('is_deleted', '=', config('const.FLG_OFF'));
        return $query;
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'location_categories', 'category_id', 'location_id');
    }

}
