<?php

namespace App\Services;

use App\Account;
use App\Location;
use App\LocationReport;
use App\Review;
use App\ReviewAggregate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class HomeService
{
    public function findAccounts()
    {
        $accounts = $this->getAccounts()
            ->select('account_id', 'gmb_account_name')
            ->pluck('gmb_account_name', 'account_id')
            ->all();

        return $accounts;
    }

    public function getSelectDateRanges(Request $request)
    {
        if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
            $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
        } else {
            $contract_type = Auth::user()->getContractType() ?? 1;
        }
        $dateRanges = config('formConst.DASHBOARD_DATE_RANGE');
        $selectDateRanges = array();
        foreach($dateRanges as $key => $value) {
            if ($contract_type == 1) {
                if ($key > 0 && $key < 5) {
                   $selectDateRanges[$key] = $value;
                }
            }
        }

        return $selectDateRanges;
    }

    public function getTiles(Request $request)
    {
        $dateRanges = $this->getDateRanges($request);
        if (is_null($dateRanges['startDate'])) {
            $dateRangeStartDate = '-';
        } else {
            $dateRangeStartDate = $dateRanges['startDate'];
        }
        if (is_null($dateRanges['endDate'])) {
            $dateRangeEndDate = '-';
        } else {
            $dateRangeEndDate = $dateRanges['endDate'];
        }

        $locations = $this->getLocations()
            ->where('account_id', $request->input('account'))
            ->select('location_id')
            ->pluck('location_id')
            ->all();

        $reportUpdateDate = LocationReport::byLocations($locations)
            ->select(
                DB::raw(
                    'max(aggregate_date) as max_aggregate_date'
                )
            )
            ->first();
        if (is_null($reportUpdateDate->max_aggregate_date) || (!$dateRanges['startDate'] && !$dateRanges['endDate'])) {
            $locationReportUpdatedDate = '-';
        } else {
            $locationReportUpdatedDate = Carbon::parse($reportUpdateDate->max_aggregate_date)->format('Y-m-d');
        }

        $searchCount = $this->getConditions($locations, $dateRanges)
            ->select(
                DB::raw(
                    'sum(ifnull(gmb_queries_direct, 0)) as total_search_count_direct,
                     sum(ifnull(gmb_queries_indirect, 0)) as total_search_count_indirect'
                )
            )
            ->first();

        $totalSearchCount = $searchCount->total_search_count_direct + $searchCount->total_search_count_indirect;
        $locationCount = $this->getLocationCount($locations, $dateRanges);
        if ($locationCount == 0) {
            $totalSearchCountAverage = 0;
        } else {
            $totalSearchCountAverage = $totalSearchCount / $locationCount;
        }

        $reviewReplyUpdateDate = Review::active()
            ->byLocations($locations)
            ->select(
                DB::raw(
                    'max(gmb_create_time) as max_review_update_date,
                     max(update_time) as max_reply_update_date'
                )
            )
            ->first();
        if (is_null($reviewReplyUpdateDate->max_review_update_date) || (!$dateRanges['startDate'] && !$dateRanges['endDate'])) {
            $reviewUpdatedDate = '-';
        } else {
            $reviewUpdatedDate = Carbon::parse($reviewReplyUpdateDate->max_review_update_date)->format('Y-m-d');
        }

        if (is_null($reviewReplyUpdateDate->max_reply_update_date) || (!$dateRanges['startDate'] && !$dateRanges['endDate'])) {
            $replyUpdatedDate = $reviewUpdatedDate;
        } else {
            $replyUpdatedDate = Carbon::parse($reviewReplyUpdateDate->max_reply_update_date)->format('Y-m-d');
        }

        $totalReviewCount = $this->getReviews($locations, $dateRanges)
            ->distinct()
            ->count(['review_id']);

        $totalReviewCountUnreplied = $this->getReviews($locations, $dateRanges)
            ->reviewUnreplied()
            ->distinct()
            ->count(['reviews.review_id']);

        $tiles = array(
            'dateRangeStartDate'        => $dateRangeStartDate,
            'dateRangeEndDate'          => $dateRangeEndDate,
            'locationReportUpdatedDate' => $locationReportUpdatedDate,
            'totalSearchCount'          => number_format($totalSearchCount),
            'totalSearchCountAverage'   => number_format($totalSearchCountAverage),
            'reviewUpdatedDate'         => $reviewUpdatedDate,
            'totalReviewCount'          => number_format($totalReviewCount),
            'replyUpdatedDate'          => $replyUpdatedDate,
            'totalReviewCountUnreplied' => number_format($totalReviewCountUnreplied)
        );

        saveMyBrandToSessionCookie($request->account);

        return $tiles;
    }

    public function getCharts(Request $request)
    {
        $searchCount = $this->getSearchCountOfChart($request);

        $aggregateDate = array();
        $totalSearchCount = array();
        $totalSearchCountAverage = array();
        $totalSearchCountDirect = array();
        $totalSearchCountIndirect = array();
        $totalSearchCountChain = array();
        $totalSearchCountDirectRatio = array();
        $totalSearchCountIndirectRatio = array();
        $totalSearchCountChainRatio = array();
        $totalActionCount = array();
        $totalActionCountWebsite = array();
        $totalActionCountPhone = array();
        $totalActionCountDrivingDirections = array();
        $totalActionCountWebsiteRatio = array();
        $totalActionCountPhoneRatio = array();
        $totalActionCountDrivingDirectionsRatio = array();
        $totalActionCountRatio = array();
        $totalNotActionCountRatio = array();
        if ($searchCount->isEmpty()) {
            $aggregateDate[] = '';
            $totalSearchCount[] = 0;
            $totalSearchCountAverage[] = 0;
            $totalSearchCountDirect[] = 0;
            $totalSearchCountIndirect[] = 0;
            $totalSearchCountChain[] = 0;
            $totalSearchCountDirectRatio[] = 0;
            $totalSearchCountIndirectRatio[] = 0;
            $totalSearchCountChainRatio[] = 0;
            $totalActionCount[] = 0;
            $totalActionCountWebsite[] = 0;
            $totalActionCountPhone[] = 0;
            $totalActionCountDrivingDirections[] = 0;
            $totalActionCountWebsiteRatio[] = 0;
            $totalActionCountPhoneRatio[] = 0;
            $totalActionCountDrivingDirectionsRatio[] = 0;
            $totalActionCountRatio[] = 0;
            $totalNotActionCountRatio[] = 0;
        } else {
            foreach($searchCount as $count) {
                if ($request->dateRange == '4' || $request->dateRange == '5') {
                    $aggregateDate[] = Carbon::parse($count->aggregate_date)->format('Y年m月');
                } else {
                    $aggregateDate[] = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                }

                $tempTotalSearchCountDirect = $count->total_search_count_direct;
                $tempTotalSearchCountIndirectAndChain = $count->total_search_count_indirect;
                $tempTotalSearchCountChain = $count->total_search_count_chain;
                $tempTotalSearchCount = $tempTotalSearchCountDirect + $tempTotalSearchCountIndirectAndChain;
                $totalSearchCount[] = $tempTotalSearchCount;

                $locationCount = $count->location_count;
                if ($locationCount == 0) {
                    $totalSearchCountAverage[] = 0;
                } else {
                    $totalSearchCountAverage[] = round($tempTotalSearchCount / $locationCount);
                }

                $tempTotalSearchCountIndirect = $tempTotalSearchCountIndirectAndChain - $tempTotalSearchCountChain;
                $totalSearchCountDirect[] = $tempTotalSearchCountDirect;
                $totalSearchCountIndirect[] = $tempTotalSearchCountIndirect;
                $totalSearchCountChain[] = $tempTotalSearchCountChain;

                if ($tempTotalSearchCount == 0) {
                    $tempTotalSearchCountDirectRatio = 0;
                    $tempTotalSearchCountChainRatio = 0;
                    $totalSearchCountIndirectRatio[] = 0;
                } else {
                    $tempTotalSearchCountDirectRatio = round(($tempTotalSearchCountDirect / $tempTotalSearchCount) * 100);
                    $tempTotalSearchCountChainRatio = round(($tempTotalSearchCountChain / $tempTotalSearchCount) * 100);
                    $totalSearchCountIndirectRatio[] = 100 - $tempTotalSearchCountDirectRatio - $tempTotalSearchCountChainRatio;
                }
                $totalSearchCountDirectRatio[] = $tempTotalSearchCountDirectRatio;
                $totalSearchCountChainRatio[] = $tempTotalSearchCountChainRatio;

                $tempTotalActionCountWebsite = $count->total_action_count_website;
                $tempTotalActionCountPhone = $count->total_action_count_phone;
                $tempTotalActionCountDrivingDirections = $count->total_action_count_driving_directions;
                $tempTotalActionCount = $tempTotalActionCountWebsite + $tempTotalActionCountPhone + $tempTotalActionCountDrivingDirections;
                $totalActionCount[] = $tempTotalActionCount;

                $totalActionCountWebsite[] = $tempTotalActionCountWebsite;
                $totalActionCountPhone[] = $tempTotalActionCountPhone;
                $totalActionCountDrivingDirections[] = $tempTotalActionCountDrivingDirections;

                if ($tempTotalActionCount == 0) {
                    $tempTotalActionCountPhoneRatio = 0;
                    $tempTotalActionCountDrivingDirectionsRatio = 0;
                    $totalActionCountWebsiteRatio[] = 0;
                } else {
                    $tempTotalActionCountPhoneRatio = round(($tempTotalActionCountPhone / $tempTotalActionCount) * 100);
                    $tempTotalActionCountDrivingDirectionsRatio = round(($tempTotalActionCountDrivingDirections / $tempTotalActionCount) * 100);
                    $totalActionCountWebsiteRatio[] = 100 - $tempTotalActionCountPhoneRatio - $tempTotalActionCountDrivingDirectionsRatio;
                }
                $totalActionCountPhoneRatio[] = $tempTotalActionCountPhoneRatio;
                $totalActionCountDrivingDirectionsRatio[] = $tempTotalActionCountDrivingDirectionsRatio;

                if ($tempTotalSearchCount == 0) {
                    $tempTotalActionCountRatio = 0;
                    $totalNotActionCountRatio[] = 0;
                } else {
                    $tempTotalActionCountRatio = round(($tempTotalActionCount / $tempTotalSearchCount) * 100);
                    $totalNotActionCountRatio[] = 100 - $tempTotalActionCountRatio;
                }
                $totalActionCountRatio[] = $tempTotalActionCountRatio;
            }
        }

        $charts = array(
            'aggregateDate'                          => $aggregateDate,
            'totalSearchCount'                       => $totalSearchCount,
            'totalSearchCountAverage'                => $totalSearchCountAverage,
            'totalSearchCountDirect'                 => $totalSearchCountDirect,
            'totalSearchCountIndirect'               => $totalSearchCountIndirect,
            'totalSearchCountChain'                  => $totalSearchCountChain,
            'totalSearchCountDirectRatio'            => $totalSearchCountDirectRatio,
            'totalSearchCountIndirectRatio'          => $totalSearchCountIndirectRatio,
            'totalSearchCountChainRatio'             => $totalSearchCountChainRatio,
            'totalActionCount'                       => $totalActionCount,
            'totalActionCountWebsite'                => $totalActionCountWebsite,
            'totalActionCountPhone'                  => $totalActionCountPhone,
            'totalActionCountDrivingDirections'      => $totalActionCountDrivingDirections,
            'totalActionCountWebsiteRatio'           => $totalActionCountWebsiteRatio,
            'totalActionCountPhoneRatio'             => $totalActionCountPhoneRatio,
            'totalActionCountDrivingDirectionsRatio' => $totalActionCountDrivingDirectionsRatio,
            'totalActionCountRatio'                  => $totalActionCountRatio,
            'totalNotActionCountRatio'               => $totalNotActionCountRatio
        );

        saveMyBrandToSessionCookie($request->account);

        return $charts;
    }

    public function export(Request $request)
    {
        $searchCount = $this->getSearchCountOfChart($request);

        $chart = array();
        if ($searchCount->isEmpty()) {
            $chart = [];
        } else {
            switch ($request->chartType) {
                case '0':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $tempTotalSearchCount = $count->total_search_count_direct + $count->total_search_count_indirect;
                        $totalSearchCount = $tempTotalSearchCount;

                        $locationCount = $count->location_count;
                        if ($locationCount == 0) {
                            $totalSearchCountAverage = 0;
                        } else {
                            $totalSearchCountAverage = round($tempTotalSearchCount / $locationCount);
                        }

                        $chartData = array(
                            'aggregate_date'             => $aggregateDate,
                            'total_search_count'         => $totalSearchCount,
                            'total_search_count_average' => $totalSearchCountAverage
                        );

                        $chart[] = $chartData;
                    }
                    break;

                case '1':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $totalSearchCountDirect = $count->total_search_count_direct;
                        $totalSearchCountIndirect = $count->total_search_count_indirect - $count->total_search_count_chain;
                        $totalSearchCountChain = $count->total_search_count_chain;

                        $chartData = array(
                            'aggregate_date'              => $aggregateDate,
                            'total_search_count_direct'   => $totalSearchCountDirect,
                            'total_search_count_indirect' => $totalSearchCountIndirect,
                            'total_search_count_chain'    => $totalSearchCountChain
                        );

                        $chart[] = $chartData;
                    }
                    break;

                case '2':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $tempTotalSearchCount = $count->total_search_count_direct + $count->total_search_count_indirect;
                        $tempTotalSearchCountDirect = $count->total_search_count_direct;
                        $tempTotalSearchCountChain = $count->total_search_count_chain;
                        if ($tempTotalSearchCount == 0) {
                            $tempTotalSearchCountDirectRatio = 0;
                            $tempTotalSearchCountChainRatio = 0;
                            $totalSearchCountIndirectRatio = 0;
                        } else {
                            $tempTotalSearchCountDirectRatio = round(($tempTotalSearchCountDirect / $tempTotalSearchCount) * 100);
                            $tempTotalSearchCountChainRatio = round(($tempTotalSearchCountChain / $tempTotalSearchCount) * 100);
                            $totalSearchCountIndirectRatio = 100 - $tempTotalSearchCountDirectRatio - $tempTotalSearchCountChainRatio;
                        }
                        $totalSearchCountDirectRatio = $tempTotalSearchCountDirectRatio;
                        $totalSearchCountChainRatio = $tempTotalSearchCountChainRatio;

                        $chartData = array(
                            'aggregate_date'                    => $aggregateDate,
                            'total_search_count_direct_ratio'   => $totalSearchCountDirectRatio,
                            'total_search_count_indirect_ratio' => $totalSearchCountIndirectRatio,
                            'total_search_count_chain_ratio'    => $totalSearchCountChainRatio
                        );

                        $chart[] = $chartData;
                    }
                    break;

                case '3':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $totalActionCountWebsite = $count->total_action_count_website;
                        $totalActionCountPhone = $count->total_action_count_phone;
                        $totalActionCountDrivingDirections = $count->total_action_count_driving_directions;

                        $chartData = array(
                            'aggregate_date'                        => $aggregateDate,
                            'total_action_count_website'            => $totalActionCountWebsite,
                            'total_action_count_phone'              => $totalActionCountPhone,
                            'total_action_count_driving_directions' => $totalActionCountDrivingDirections
                        );

                        $chart[] = $chartData;
                    }
                    break;

                case '4':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $tempTotalActionCount = $count->total_action_count_website + $count->total_action_count_phone + $count->total_action_count_driving_directions;
                        $tempTotalActionCountPhone = $count->total_action_count_phone;
                        $tempTotalActionCountDrivingDirections = $count->total_action_count_driving_directions;
                        if ($tempTotalActionCount == 0) {
                            $tempTotalActionCountPhoneRatio = 0;
                            $tempTotalActionCountDrivingDirectionsRatio = 0;
                            $totalActionCountWebsiteRatio = 0;
                        } else {
                            $tempTotalActionCountPhoneRatio = round(($tempTotalActionCountPhone / $tempTotalActionCount) * 100);
                            $tempTotalActionCountDrivingDirectionsRatio = round(($tempTotalActionCountDrivingDirections / $tempTotalActionCount) * 100);
                            $totalActionCountWebsiteRatio = 100 - $tempTotalActionCountPhoneRatio - $tempTotalActionCountDrivingDirectionsRatio;
                        }
                        $totalActionCountPhoneRatio = $tempTotalActionCountPhoneRatio;
                        $totalActionCountDrivingDirectionsRatio = $tempTotalActionCountDrivingDirectionsRatio;

                        $chartData = array(
                            'aggregate_date'                              => $aggregateDate,
                            'total_action_count_website_ratio'            => $totalActionCountWebsiteRatio,
                            'total_action_count_phone_ratio'              => $totalActionCountPhoneRatio,
                            'total_action_count_driving_directions_ratio' => $totalActionCountDrivingDirectionsRatio
                        );

                        $chart[] = $chartData;
                    }
                    break;

                case '5':
                    foreach($searchCount as $count) {
                        if ($request->dateRange == '4' || $request->dateRange == '5') {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月');
                        } else {
                            $aggregateDate = Carbon::parse($count->aggregate_date)->format('Y年m月d日');
                        }

                        $tempTotalSearchCount = $count->total_search_count_direct + $count->total_search_count_indirect;
                        $tempTotalActionCount = $count->total_action_count_website + $count->total_action_count_phone + $count->total_action_count_driving_directions;
                        if ($tempTotalSearchCount == 0) {
                            $totalActionCountRatio = 0;
                        } else {
                            $totalActionCountRatio = round(($tempTotalActionCount / $tempTotalSearchCount) * 100);
                        }

                        $chartData = array(
                            'aggregate_date'           => $aggregateDate,
                            'total_action_count_ratio' => $totalActionCountRatio
                        );

                        $chart[] = $chartData;
                    }
                    break;
            }
        }

        return outPutCsv($chart, config('const.CSV_HEADERS.DASHBOARD_CHART')[$request->chartType], config('const.EXPORT_DASHBOARD_CHART_FILE_NAME') . '_' . date('ymd'));
    }

    public function getLists(Request $request)
    {
        $dateRanges = $this->getDateRanges($request);

        $locations = $this->getLocations()
            ->where('account_id', $request->input('account'))
            ->pluck('location_id')
            ;
        $locationSearchList = Location::byLocations($locations)
            ->select('location_id', 'gmb_location_name')
            ->groupBy('location_id', 'gmb_location_name')
            ->orderBy('location_id')
            ->get();
        $locationSearchCount = LocationReport::byLocations($locations)
            ->betweenAggregateDate($dateRanges['startDate'], $dateRanges['endDate'] . ' 23:59:59')
            ->select(
                'location_id',
                DB::raw(
                    'sum(ifnull(gmb_queries_direct, 0)) as location_search_count_direct,
                     sum(ifnull(gmb_queries_indirect, 0)) as location_search_count_indirect,
                     sum(ifnull(gmb_queries_chain, 0)) as location_search_count_chain,
                     sum(ifnull(gmb_actions_website, 0)) as location_action_count_website,
                     sum(ifnull(gmb_actions_phone, 0)) as location_action_count_phone,
                     sum(ifnull(gmb_actions_driving_directions, 0)) as location_action_count_driving_directions'
                )
            )
            ->groupBy('location_id')
            ->orderBy('location_id')
            ->get();

        $locationAverageRating = ReviewAggregate::byLocations($locations)
            ->select(
                'location_id',
                'gmb_average_rating'
            )
            ->orderBy('location_id')
            ->get();

        $locationReviewCount = $this->getReviews($locations, $dateRanges)
            ->select(
                'location_id',
                DB::raw(
                    'count(review_id) as location_review_count'
                )
            )
            ->groupBy('location_id')
            ->orderBy('location_id')
            ->get();

        $locationReviewCountUnreplied = $this->getReviews($locations, $dateRanges)
            ->reviewUnreplied()
            ->select(
                'reviews.location_id',
                DB::raw(
                    'count(reviews.review_id) as location_review_count_unreplied'
                )
            )
            ->groupBy('location_id')
            ->orderBy('location_id')
            ->get();

        $lists = new Collection();
        $keys = collect([
            'location_id',
            'location_name',
            'location_search_count_total',
            'location_search_count_direct',
            'location_search_count_indirect',
            'location_search_count_chain',
            'location_actions_count',
            'location_average_rating',
            'location_review_count',
            'location_review_count_unreplied'
        ]);
        foreach($locationSearchList as $location) {
            $location_search_count_total = 0;
            $location_search_count_direct = 0;
            $location_search_count_indirect = 0;
            $location_search_count_chain = 0;
            $location_actions_count = 0;
            $location_average_rating = 0;
            $location_review_count = 0;
            $location_review_count_unreplied = 0;

            $location_id = $location->location_id;
            $location_name = $location->gmb_location_name;

            $tempLocationSearchCount = $locationSearchCount->where('location_id', $location->location_id)->first();
            if (!is_null($tempLocationSearchCount)) {
                $location_search_count_total = $tempLocationSearchCount->location_search_count_direct + $tempLocationSearchCount->location_search_count_indirect;
                $location_search_count_direct = $tempLocationSearchCount->location_search_count_direct;
                $location_search_count_indirect = $tempLocationSearchCount->location_search_count_indirect - $tempLocationSearchCount->location_search_count_chain;
                $location_search_count_chain = $tempLocationSearchCount->location_search_count_chain;
                $location_actions_count = $tempLocationSearchCount->location_action_count_website + $tempLocationSearchCount->location_action_count_phone + $tempLocationSearchCount->location_action_count_driving_directions;
            }

            $tempLocationAverageRating = $locationAverageRating->where('location_id', $location->location_id)->first();
            if (!is_null($tempLocationAverageRating) && $dateRanges['startDate'] && $dateRanges['endDate']) {
                $location_average_rating = $tempLocationAverageRating->gmb_average_rating;
            }

            $tempLocationReviewCount = $locationReviewCount->where('location_id', $location->location_id)->first();
            if (!is_null($tempLocationReviewCount)) {
                $location_review_count = $tempLocationReviewCount->location_review_count;
            }

            $tempLocationReviewCountUnreplied = $locationReviewCountUnreplied->where('location_id', $location->location_id)->first();
            if (!is_null($tempLocationReviewCountUnreplied)) {
                $location_review_count_unreplied = $tempLocationReviewCountUnreplied->location_review_count_unreplied;
            }

            $list = $keys
                ->combine([
                    $location_id,
                    trim(mb_convert_kana($location_name, 's', 'UTF-8')),
                    number_format($location_search_count_total),
                    number_format($location_search_count_direct),
                    number_format($location_search_count_indirect),
                    number_format($location_search_count_chain),
                    number_format($location_actions_count),
                    number_format($location_average_rating, 1),
                    number_format($location_review_count),
                    number_format($location_review_count_unreplied)
                ])
                ->all();

            $lists->push($list);
        }

        saveMyBrandToSessionCookie($request->account);

        return $lists;
    }

    private function getLocationCount($locations, $dateRanges)
    {
        $locationCount = $this->getConditions($locations, $dateRanges)
            ->distinct()
            ->count(['location_id']);

        return $locationCount;
    }

    private function getSearchCountOfChart(Request $request)
    {
        $dateRanges = $this->getDateRanges($request);

        $locations = $this->getLocations()
            ->where('account_id', $request->input('account'))
            ->select('location_id')
            ->pluck('location_id')
            ->all();

        $searchCount = $this->getConditions($locations, $dateRanges)
            ->select(
                DB::raw(
                    'count(distinct location_id) as location_count,
                     sum(ifnull(gmb_queries_direct, 0)) as total_search_count_direct,
                     sum(ifnull(gmb_queries_indirect, 0)) as total_search_count_indirect,
                     sum(ifnull(gmb_queries_chain, 0)) as total_search_count_chain,
                     sum(ifnull(gmb_actions_website, 0)) as total_action_count_website,
                     sum(ifnull(gmb_actions_phone, 0)) as total_action_count_phone,
                     sum(ifnull(gmb_actions_driving_directions, 0)) as total_action_count_driving_directions'
                )
            );
        if ($request->dateRange == '4' || $request->dateRange == '5') {
            $searchCount = $searchCount
                ->addSelect([
                    DB::raw(
                        'date_format(aggregate_date, "%Y-%m-01") as aggregate_date'
                    )
                ])
                ->groupBy(
                    DB::raw(
                        'date_format(aggregate_date, "%Y-%m-01")'
                    )
                );
        } else {
            $searchCount = $searchCount
                ->addSelect([
                    DB::raw(
                        'date_format(aggregate_date, "%Y-%m-%d") as aggregate_date'
                    )
                ])
                ->groupBy(
                    DB::raw(
                        'date_format(aggregate_date, "%Y-%m-%d")'
                    )
                );
        }
        $searchCount = $searchCount
            ->orderBy('aggregate_date')
            ->get();

        return $searchCount;
    }

    private function getConditions($locations, $dateRanges)
    {
        $reports = LocationReport::byLocations($locations)
            ->betweenAggregateDate($dateRanges['startDate'], $dateRanges['endDate'] . ' 23:59:59');

        return $reports;
    }

    private function getReviews($locations, $dateRanges)
    {
        $reviews = Review::active()
            ->byLocations($locations)
            ->betweenCreateDate($dateRanges['startDate'], $dateRanges['endDate'] . ' 23:59:59');

        return $reviews;
    }

    private function getAccounts()
    {
        $accounts = Account::getMyAccounts(Auth::id());

        return $accounts;
    }

    private function getLocations()
    {
        $locations = Location::getMyLocations(Auth::id());
        
        return $locations;
    }

    private function getDateRanges(Request $request)
    {
        switch ($request->dateRange) {
            case '0':
                $inputStartDate = Carbon::parse($request->input('startDate'));
                $tempStartDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_MINDATE') + config('const.DASHBOARD_DATE_RANGE_INTERVAL') - 1));
                if ($request->filled('startDate') && $inputStartDate->gte($tempStartDate)) {
                    $startDate = $request->input('startDate');
                } else {
                    $startDate = $tempStartDate->format('Y-m-d');
                }
                $inputEndDate = Carbon::parse($request->input('endDate'));
                $tempEndDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_MAXDATE') + config('const.DASHBOARD_DATE_RANGE_INTERVAL')));
                if ($request->filled('endDate') && $inputEndDate->lte($tempEndDate)) {
                    $endDate = $request->input('endDate');
                } else {
                    $endDate = $tempEndDate->format('Y-m-d');
                }
                break;

            case '1':
            case '2':
            case '3':
                $startDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_VALUE')[$request->dateRange] + config('const.DASHBOARD_DATE_RANGE_INTERVAL') - 1))->format('Y-m-d');
                $endDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_MAXDATE') + config('const.DASHBOARD_DATE_RANGE_INTERVAL')))->format('Y-m-d');
                break;

            case '4':
            case '5':
                $startDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_INTERVAL') - 1)->subMonth(config('const.DASHBOARD_DATE_RANGE_VALUE')[$request->dateRange]))->format('Y-m-d');
                $endDate = Carbon::parse(Carbon::today()->subDays(config('const.DASHBOARD_DATE_RANGE_MAXDATE') + config('const.DASHBOARD_DATE_RANGE_INTERVAL')))->format('Y-m-d');
                break;

            default:
                $startDate = '';
                $endDate = '';
        }

        $dateRanges = array(
            'startDate' => $startDate,
            'endDate'   => $endDate
        );

        return $dateRanges;
    }
}
