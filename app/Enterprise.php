<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    protected $hidden = [];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $dates   = ['create_time', 'update_time'];

    protected $primaryKey = 'enterprise_id';

    protected $attributes = [
        'is_deleted' => 0,
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'enterprise_id', 'enterprise_id');
    }
}
