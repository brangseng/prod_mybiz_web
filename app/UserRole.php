<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class UserRole extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'user_role_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'account_id',
        'location_id',
        'role_id',
        'create_user_id',
        'update_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [
        'create_time',
        'update_time'
    ];

    public function scopeGetUserRole($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    public function scopeGetMyAccounts($query, $user_id): Collection
    {
        return $query->where('user_id', '=', $user_id)->pluck('account_id');
    }
}
