<?php

namespace App\Services;

use App\Account;
use App\Enums\SyncType;
use App\LocalPost;
use App\LocalPostGroup;
use App\Location;
use App\MediaItem;
use App\UserRole;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Util;
use Intervention\Image\Facades\Image;

class LocalPostGroupService
{
    /**
     * 指定された local_post_group_id を持つ、最初の local_post を返す。
     */
    public function findFirstLocalPost($local_post_group_id): LocalPost
    {
        return LocalPostGroup::findOrFail($local_post_group_id)->localPosts()->first();
    }

    public function findAccounts()
    {
        return Account::GetMyAccounts(Auth::id())
            ->select('account_id', 'gmb_account_name')
            ->pluck('gmb_account_name', 'account_id');
    }

    public function search(Request $request)
    {
        if (is_null($request->input('account'))) {
            $request->merge([
                'account' => session(config('formConst.SESSION_MY_BRAND'))
                    ?? UserRole::GetUserRole(Auth::id())->select('account_id')->first()->value('account_id')
            ]);
        }
        $localPostGroups = $this->getCondition($request)->orderBy('update_time', 'desc');
        
        saveMyBrandToSessionCookie($request->account);
        return $localPostGroups;
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->input('gmb_action_type') == 'LOCAL_POST_ACTION_TYPE_UNSPECIFIED') {
                $request->merge(['gmb_action_type' => null]);   // 未選択にする
            }
            $this->setSyncStatus($request); // $request内にsync_status をセットする
            $localpost_group = $this->updateOrCreateLocalPostGroup($request);
            DB::commit();
            saveMyBrandToSessionCookie($request->account_id);
            return $localpost_group;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    private function getRandomStr($length)
    {
        return substr(bin2hex(random_bytes($length)), 0, $length);
    }

    public function update(Request $request)
    {
        try {
            // 新規登録と修正の処理を共通化した
            return $this->store($request);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateOrCreateLocalPostGroup(Request $request)
    {
        try {
            // $request に必要なもの、不要なものなど混ざっているため、寄り分けて整備した配列を用意する
            $filtered_request_array = $this->getFilteredRequestArray($request);
            $filtered_request_array['gmb_account_id'] = Account::findOrFail($request->account_id)->gmb_account_id;
            $filtered_request_array['gmb_language_code'] = 'ja';
            // 画像の変更が必要かどうか？
            // $request->file() に値が入っていれば、新しい画像が指定されている
            $isUploadImages = ($request->file('upload_files') ? true : false);
            if ($filtered_request_array['local_post_group_id']) {
                // 修正
                $filtered_request_array['update_user_id'] = Auth::id();
                $localpost_group = LocalPostGroup::findOrFail($filtered_request_array['local_post_group_id']);
                $localpost_group->fill($filtered_request_array)->save();
            } else {
                // 新規登録
                $filtered_request_array['create_user_id'] = Auth::id();
                $filtered_request_array['update_user_id'] = Auth::id();
                $localpost_group = LocalPostGroup::create($filtered_request_array);
            }
            // local_post_group_id をセットする
            $filtered_request_array['local_post_group_id'] = $localpost_group->id;

            // 【 画像に関する処理 】
            // 各カラムにセットする値は request から来た値を使いまわすが、
            // API同期ステータスが「投稿」とズレるため（画像のみ PATCH にするなど）
            // request を複製した配列を使用する。
            // [TODO]   現時点ではAPIで弾かれるため、投稿：画像は1:1となっている。
            //          ただし、画像無しの場合は media_items レコードは無し
            $media_item_values = $filtered_request_array;
            // 同一の local_post_group_id 内で同じ画像URLを使用するのでURLを決定しておく
            if ($isUploadImages) {
                // 画像の保存pathを決定する
                $upload_path = $this->getUploadPath($filtered_request_array);
                // 画像の保存ファイル名を決定する
                $upload_filenames = $this->getUploadFilenames($request->file('upload_files'), $upload_path);
                $ext = pathinfo($upload_filenames[0], PATHINFO_EXTENSION);
                if ($ext == 'mp4' || $ext == 'mpeg' || $ext == 'mpg' || $ext == 'avi' || $ext == 'mov') {
                    $media_item_values['gmb_media_format'] = "VIDEO";
                } else {
                    $media_item_values['gmb_media_format'] = "PHOTO";
                }
                $media_item_values['s3_object_url']
                    = $upload_path . '/' . $upload_filenames[0];
                $media_item_values['gmb_source_url']
                    = $upload_filenames[0]
                    ? Storage::url($upload_path . '/' . $upload_filenames[0])
                    : null;
            }

            // [TODO] 現状、投稿先店舗のチェックが外されたときの削除は考慮されていない
            foreach ($request->location_id as $location_id) {
                $filtered_request_array['location_id'] = $location_id;
                $filtered_request_array['gmb_location_id'] = Location::findOrFail($location_id)->gmb_location_id;
                $localpost = LocalPost::updateOrCreate(
                    [
                        'local_post_group_id' => $localpost_group->id,
                        'location_id' => $location_id
                    ],
                    $filtered_request_array
                );

                // 新規登録された local_posts の localpost_id を media_items にセットする
                $media_item_values['local_post_id'] = $localpost->local_post_id;
                // media_items のフィールドのうち、店舗ごとに変わるものもこのループ内でセットする
                $media_item_values['gmb_location_id'] = $filtered_request_array['gmb_location_id'];
                // 写真更新時に以前のデータを削除する必要があるため保存前に以前のデータを取得しておく
                $media_item_before_save = $localpost->mediaItems()->first();
                // 写真無しで初回投稿→修正で写真追加、の場合に create_user_id をセットする
                if (!$media_item_before_save) {
                    $filtered_request_array['create_user_id'] = Auth::id();
                }

                // 写真なしの場合は media_items にデータを生成しないようにする
                if ($localpost->mediaItems()->exists() || $isUploadImages) {
                    $localpost->mediaItems()->updateOrCreate(
                        [
                            'local_post_id' => $localpost->local_post_id
                        ],
                        $media_item_values
                    );
                }

                // アップロードが必要な場合（新規で写真あり、もしくは修正時に写真変更）のみurl変更。
                // （修正で写真が変更されなかった場合も media_items.sync_* フィールドの更新は必要）
                if ($isUploadImages) {
                    if ($media_item_before_save) {
                        // 既に media_items が存在していた場合
                        // 既存の画像がS3に存在する場合は削除する
                        if ($media_item_before_save->s3_object_url && Storage::exists($media_item_before_save->s3_object_url)) {
                            Storage::delete($media_item_before_save->s3_object_url);
                        }
                        if ($media_item_before_save->s3_object_url && Storage::exists($this->getThumbnailFilenames($media_item_before_save->s3_object_url))) {
                            Storage::delete($this->getThumbnailFilenames($media_item_before_save->s3_object_url));
                        }
                    }

                    // 画像ファイルをS3にアップロードする
                    $this->uploadFiles(
                        $upload_path,
                        $upload_filenames,
                        $request->file('upload_files')
                    );
                }
            }

            return $localpost_group;
        } catch (Exception $e) {
            if ($isUploadImages) {
                foreach ($upload_filenames as $uploaded_file) {
                    if (Storage::exists($uploaded_file)) {
                        Storage::delete($uploaded_file);
                    }
                    if (Storage::exists($this->getThumbnailFilenames($uploaded_file))) {
                        Storage::delete($this->getThumbnailFilenames($uploaded_file));
                    }
                }
            }
            throw $e;
        }
    }

    private function setSyncStatus($request)
    {
        // https://paraworks.backlog.com/view/GMB-8#comment-26612468
        // ・投稿時刻を選択せずに登録した場合は、下書き状態ということでDRAFTを設定する。
        // ・投稿時刻を選択して登録した場合は、編集完了ということでQUEUEDを設定する。
        // 詳細 → https://docs.google.com/spreadsheets/d/1AsH_BVtmOnsxoQYhBaJg93lh8Dvd7J_u4IV9hhO7S_4/edit#gid=0
        $first_local_post
            = $request->local_post_group_id ? LocalPostGroup::getFirstLocalPost($request->local_post_group_id) : null;
        $sync_status = null;
        $scheduled_sync_time = null;
        // APIからみた「新規」か「修正」かをセットする
        $sync_type = $first_local_post
            && LocalPost::find($first_local_post->local_post_id)->gmb_local_post_id ? "PATCH" : "CREATE";

        if ($request->is_scheduled) {
            // 投稿予約 ＝ API連携日時確定
            $sync_status = 'QUEUED';
            $scheduled_sync_time = Carbon::parse($request->scheduled_sync_time
                . ' ' . $request->scheduled_range)->format('Y-m-d H:i:s');
        } else {
            // 下書き
            $sync_status = 'DRAFT';
            $scheduled_sync_time = null;
        }
        $request->merge(['sync_type' => $sync_type]);
        $request->merge(['sync_status' => $sync_status]);
        $request->merge(['scheduled_sync_time' => $scheduled_sync_time]);
    }

    private function getFilteredRequestArray($request): array
    {
        // カラム名のずれがあるため調整
        if ($request->gmb_topic_type) {
            $request->merge(['topic_type' => $request->gmb_topic_type]);
        }
        if ($request->gmb_event_title) {
            $request->merge(['event_title' => $request->gmb_event_title]);
        }
        if ($request->gmb_event_start_time) {
            $request->merge(['event_start_time' => (new Carbon($request->gmb_event_start_time))
                ->format(config('formConst.FORMAT_DATETIME_YMDHIS'))]);
        }
        if ($request->gmb_event_end_time) {
            $request->merge(['event_end_time' => (new Carbon($request->gmb_event_end_time))
                ->format(config('formConst.FORMAT_DATETIME_YMDHIS'))]);
        }

        // 日付項目のフォーマット調整
        if ($request->gmb_event_start_time) {
            $request->merge(['gmb_event_start_time' => (new Carbon($request->gmb_event_start_time))
                ->format(config('formConst.FORMAT_DATETIME_YMDHIS'))]);
        }
        if ($request->gmb_event_end_time) {
            $request->merge(['gmb_event_end_time' => (new Carbon($request->gmb_event_end_time))
                ->format(config('formConst.FORMAT_DATETIME_YMDHIS'))]);
        }

        // 投稿日時がチェックされていなかった場合の調整
        if ($request->is_scheduled != 1) {
            $request->scheduled_sync_time = null;
        }

        $filtered_array = array_filter($request->all(), function ($value, $key) {
            // 配列・Object は削除。
            $isArray = is_array($value);
            $isObject = is_object($value);
            // 日付カラムは空文字列で更新しようとするとエラーになるため、空文字の場合は request から削除したい
            $date_fields = [
                'gmb_event_start_time', 'gmb_event_end_time', /*'scheduled_sync_time',*/
                'sync_time'
            ];
            $isBlankDate = in_array($key, $date_fields) && empty($value);
            return !$isArray && !$isObject && !$isBlankDate;
        }, ARRAY_FILTER_USE_BOTH);

        // TOPIC_TYPE による調整（不要な項目を削除する）
        switch ($filtered_array['topic_type']) {
            case "STANDARD":
                $filtered_array['gmb_event_title'] = null;
                $filtered_array['gmb_event_start_time'] = null;
                $filtered_array['gmb_event_end_time'] = null;
                $filtered_array['gmb_offer_coupon_code'] = null;
                $filtered_array['gmb_offer_redeem_online_url'] = null;
                $filtered_array['gmb_offer_terms_conditions'] = null;
                break;
            case "EVENT":
                $filtered_array['gmb_offer_coupon_code'] = null;
                $filtered_array['gmb_offer_redeem_online_url'] = null;
                $filtered_array['gmb_offer_terms_conditions'] = null;
                break;
            case "OFFER":
                $filtered_array['gmb_action_type'] = null;
                $filtered_array['gmb_action_type_url'] = null;
                break;
        }

        return $filtered_array;
    }

    public function getCondition(Request $request)
    {
        $account = $request->input('account');
        // 契約タイプにより参照可能な期間が異なる
        $contract_type_period = config('const.CONTRACT_TYPE_PERIOD')[Auth::user()->getContractType() ?? 1];
        $start_date = null;
        if (0 == $contract_type_period) {
            // 契約タイプにより、参照可能期間の制限なし
            $start_date = $request->input('stDate')
                ? Carbon::parse($request->input('stDate'))
                : null;
        } else {
            // 契約タイプにより、参照可能期間の制限あり
            $period_start = Carbon::now()->subMonths($contract_type_period)->ceilDay();
            $start_date = $request->input('stDate')
                ? Carbon::parse($request->input('stDate'))->max($period_start)
                : $period_start;
        }
        $end_date = $request->input('endDate') ? Carbon::parse($request->input('endDate'))->addDay() : null;
        $gmb_topic_type = $request->input('gmb_topic_type');
        $gmb_action_type = $request->input('gmb_action_type');
        $sync_status = $request->input('sync_status');

        $localPostGroups = LocalPostGroup::active()->whereIn(
            'local_post_groups.account_id',
            Account::getMyAccounts(Auth::id())->pluck('account_id')->all()
        );

        if ($account) {
            $localPostGroups = $localPostGroups->where('local_post_groups.account_id', '=', $account);
        }
        if ($start_date) {
            // $localPostGroups = $localPostGroups->where('local_post_groups.update_time', '>=', $start_date);
            $localPostGroups = $localPostGroups->whereHas('localposts', function ($query) use ($start_date, $request) {
                $query->where('local_posts.sync_time', '>=', $start_date);
                // 画面での日付範囲指定がされていない場合は日付がnullのデータも含めたい
                if (!$request->input('stDate')) {
                    $query->orWhereNull('local_posts.sync_time');
                }
            });
        }
        if ($end_date) {
            // $localPostGroups = $localPostGroups->where('local_post_groups.update_time', '<', $end_date);
            $localPostGroups = $localPostGroups->whereHas('localposts', function ($query) use ($end_date) {
                $query->where('local_posts.sync_time', '<', $end_date);
            });
        }
        if ($gmb_topic_type) {
            $localPostGroups = $localPostGroups->where('local_post_groups.topic_type', '=', $gmb_topic_type);
        }
        if ($gmb_action_type) {
            $localPostGroups = $localPostGroups->whereHas('localposts', function ($query) use ($gmb_action_type) {
                $query->where('local_posts.gmb_action_type', '=', $gmb_action_type);
            });
        }
        if ($sync_status) {
            $localPostGroups = $localPostGroups->whereHas('localposts', function ($query) use ($sync_status) {
                $query->where('local_posts.sync_status', '=', $sync_status);
            });
        }

        return $localPostGroups;
    }

    public function export(Request $request)
    {
        try {
            // サブクエリー用
            $localPosts = LocalPost::select(
                'local_post_group_id',
                DB::raw('max(sync_time) as sync_time'),
                DB::raw('max(gmb_summary) as gmb_summary'),
                DB::raw('max(gmb_action_type) as gmb_action_type'),
                DB::raw('max(sync_status) as sync_status')
            )
                ->groupBy('local_post_group_id');

            $localPostGroups = $this->getCondition($request);
            // $localPostGroups = $localPostGroups->with('account')
            //     ->select(config('const.CSV_ITEMS.LOCAL_POST_GROUP'))->get();
            $localPostGroups = $localPostGroups
                ->select(config('const.CSV_ITEMS.LOCAL_POST_GROUP'))
                ->leftJoin('accounts', 'local_post_groups.account_id', '=', 'accounts.account_id')
                ->joinSub($localPosts, 'local_posts', function ($join) {
                    $join->on('local_post_groups.id', '=', 'local_posts.local_post_group_id');
                })
                ->orderBy('local_post_groups.update_time', 'desc')
                ->get();
            // const文字列の置き換え
            $localPostGroups_converted = $localPostGroups->map(function ($item, $key) {
                $item->topic_type = config('formConst.GMB_TOPIC_TYPE')[strtoupper($item->topic_type)];
                $item->gmb_action_type = config('formConst.GMB_ACTION_TYPE')[strtoupper($item->gmb_action_type)];
                $item->sync_status = config('formConst.LOCAL_POST_SYNC_STATUS')[strtoupper($item->sync_status)];
                //$item->sync_status = SyncStatusType::getString(strtoupper($item->sync_status));
                return $item;
            });

            if (!empty($localPostGroups_converted)) {
                $localPostGroups = $localPostGroups_converted->toArray();
            } else {
                $localPostGroups = [];
            }
            return outPutCsv(
                $localPostGroups,
                config('const.CSV_HEADERS.LOCAL_POST_GROUP'),
                config('const.EXPORT_LOCAL_POSTS_FILE_NAME') . '-' . date('YmdHi')
            );
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            return false;
        }
    }

    /**
     * 画像の保存pathを決定する
     * https://paraworks.backlog.com/view/GMB-9#comment-36491842
     *
     */
    private function getUploadPath(array $filtered_request_array)
    {
        return Util::normalizePath(
            'gmb/media'
                . '/' . (mb_strtolower($filtered_request_array['gmb_topic_type'] ?? 'other'))
                . '/' . ($filtered_request_array['account_id'] ?? "no_account_id")
                . '/' . ($filtered_request_array['local_post_group_id'] ?? "no_local_post_group_id")
        );
    }

    private function getUploadFilename($upload_file)
    {
        return Util::normalizePath(
            Carbon::now()->format('YmdHis')
                . '_' . $this->getRandomStr(5)
                . '.' . $upload_file->extension()
        );
    }

    private function getUploadFilenames(array $upload_files): array
    {
        $upload_filenames = [];
        foreach ($upload_files as $index => $upload_file) {
            $upload_filenames[$index]
                = $this->getUploadFilename($upload_file);
        }
        return $upload_filenames;
    }

    private function getThumbnailFilenames($fileName): string
    {
        //拡張子の前に文字列を追加
        $pos = strrpos($fileName, '.'); // .が最後に現れる位置
        if ($pos) {
            return (substr($fileName, 0, $pos) . config('const.THUMBNAIL_PREFIX') . substr($fileName, $pos));
        } else {
            return ($fileName . config('const.THUMBNAIL_PREFIX'));
        }
    }

    /**
     * Undocumented function
     *
     * @param string $upload_path ベースとなるpath
     * @param array $upload_filenames upload_files と対になる、S3上の保存名
     * @param array $upload_files $request->file() の値
     * @return void
     */
    private function uploadFiles(string $upload_path, array $upload_filenames, array $upload_files)
    {
        if ($upload_files) {
            foreach ($upload_files as $index => $upload_file) {
                // 同一ファイルが存在していなければアップロードする
                if (!Storage::exists($upload_path . '/' . $upload_filenames[$index])) {
                    $upload_file->storeAs(
                        $upload_path,
                        $upload_filenames[$index],
                        ['ACL' => 'public-read']
                    );

                    // サムネイル画像の生成
                    $thumbnailImage = Image::make($upload_file)
                        ->resize(300, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    // S3のパスへ画像をアップロード
                    Storage::put(
                        $upload_path . '/' . $this->getThumbnailFilenames($upload_filenames[$index]),
                        (string)$thumbnailImage->encode(),
                        ['ACL' => 'public-read']
                    );
                }
            }
        }
    }

    public function destroy($local_post_group_id)
    {
        try {
            DB::beginTransaction();
            $on = config('const.FLG_ON');
            $next_sync_time = getNextSyncDateTime(Carbon::now()->addMinutes(60));
            $cols_api_synced = [
                'is_deleted' => $on,
                'sync_type' => 'DELETE',
                'sync_status' => 'QUEUED',
                // [TODO] config から猶予時間を取得する
                'scheduled_sync_time' => $next_sync_time
            ];
            $cols_queued_but_not_synced
                = ['is_deleted' => $on, 'sync_type' => 'DELETE', 'sync_status' => 'CANCEL'];
            $cols_not_synced
                = ['is_deleted' => $on, 'sync_type' => 'DELETE', 'sync_status' => 'DRAFT'];

            $localPostGroup = LocalPostGroup::findOrFail($local_post_group_id);
            $localPostGroup->is_deleted = config('const.FLG_ON');
            $localPostGroup->save();
            foreach ($localPostGroup->localPosts as $localpost) {
                if ($localpost->sync_status == 'SYNCED') {
                    // API同期済みの場合
                    $localpost->update($cols_api_synced);
                } elseif ($localpost->sync_status == 'QUEUED') {
                    // 予約済み（かつ未連携）の場合
                    $localpost->update($cols_queued_but_not_synced);
                } else {
                    // 下書きの場合
                    $localpost->update($cols_not_synced);
                }
            }
            // モデル上、media_items に local_post_id だけではなく local_post_group_id も保持している
            foreach ($localPostGroup->mediaItems as $mediaitem) {
                if ($mediaitem->sync_status == 'SYNCED') {
                    // API同期済みの場合
                    $mediaitem->update($cols_api_synced);
                } elseif ($mediaitem->sync_status == 'QUEUED') {
                    // 予約済み（かつ未連携）の場合
                    $mediaitem->update($cols_queued_but_not_synced);
                } else {
                    // 下書きの場合
                    $mediaitem->update($cols_not_synced);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}


