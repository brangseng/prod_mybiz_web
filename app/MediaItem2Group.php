<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MediaItem2Group extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'media_item2_group_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'gmb_account_id',
        'gmb_location_id',
        'gmb_media_key',
        'gmb_media_format',
        'gmb_location_association_category',
        'gmb_location_association_price_list_item_id',
        'gmb_google_url',
        'gmb_thumbnail_url',
        'gmb_create_time',
        'gmb_dimentions_width_pixels',
        'gmb_dimentions_height_pixels',
        'gmb_insights_view_count',
        'gmb_attribution_profile_name',
        'gmb_attribution_profile_photo_url',
        'gmb_attribution_takedown_url',
        'gmb_atttribution_profile_url',
        'gmb_description',
        'gmb_source_url',
        'gmb_data_ref_resource_name',
        's3_object_url',
        'is_deleted',
        'create_user_id',
        'update_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [
        'create_time',
        'update_time'
    ];

    protected $attributes = [
        'gmb_account_id' => '',
        'gmb_location_id' => '',
        'gmb_media_key' => '',
        'is_deleted' => 0
    ];

    public function isEditable()
    {
        $first_media_item = $this->mediaItems()->first();
        return ($first_media_item->sync_status == false || $first_media_item->sync_status == 'DRAFT');
    }

    public function getFileList()
    {
        $objectUrls = $this->mediaItems()->pluck('s3_object_url')->unique()->toArray();

        $fileList = [];

        foreach ($objectUrls as $objectUrl) {
            $fileUrl = $this->mediaItems()->where('s3_object_url', $objectUrl)->first()->getImageUrl();
            $thumbnailUrl = $this->mediaItems()->where('s3_object_url', $objectUrl)->first()->getThumbnailImageUrl();
            $mediaFormat = $this->mediaItems()->where('s3_object_url', $objectUrl)->first()->gmb_media_format;
            $mediaItem2Ids = $this->mediaItems()->where('s3_object_url', $objectUrl)->pluck('media_item2_id')->toArray();
            $fileList[] = [
                'fileUrl' => $fileUrl,
                'thumbnailUrl' => $thumbnailUrl,
                'fileName' => basename($fileUrl),
                'mediaFormat' => $mediaFormat,
                'mediaItem2Ids' => implode(',', $mediaItem2Ids)
            ];
        }

        return $fileList;
    }

    public function scopeActive($query)
    {
        return $query->where('media_item2_groups.is_deleted', '=', config('const.FLG_OFF'));
    }

    public function account()
    {
        return $this->belongsTo(Location::class, 'account_id', 'account_id');
    }

    public function mediaItems2()
    {
        return $this->hasMany(MediaItem2::class, 'media_item2_group_id', 'media_item2_group_id');
    }

    public function getThumbnailImageUrl()
    {
        $image_url = $this->getImageUrl();

        // 拡張子の前に文字列を追加
        // .が最後に現れる位置
        $pos = strrpos($image_url, '.');
        if ($pos && $image_url !== asset('img/no_image.png')) {
            $filePath = substr($image_url, 0, $pos) . config('const.THUMBNAIL_PREFIX') . substr($image_url, $pos);

            // S3に配置されている相対パスを取得する
            $thumPos = strrpos($this->s3_object_url, '.');
            $thumFilePath = substr($this->s3_object_url, 0, $thumPos) . config('const.THUMBNAIL_PREFIX') . substr($this->s3_object_url, $thumPos);

            // 存在すればファイルパスを更新
            if (Storage::exists($thumFilePath)) {
                $image_url = $filePath;
            }
        }

        return $image_url;
    }

    public function getImageUrl()
    {
        $image_url = asset('img/no_image.png');
        if ($this->gmb_source_url) {
            $gmb_source_urls = explode(',', $this->gmb_source_url);
            $image_url = $gmb_source_urls[0];
        }

        return $image_url;
    }

    public function mediaItems()
    {
        return $this->hasMany('App\MediaItem2', 'media_item2_group_id', 'media_item2_group_id');
    }
}
