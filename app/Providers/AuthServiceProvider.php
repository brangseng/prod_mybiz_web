<?php

namespace App\Providers;

use App\UserRole;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function ($user) {
            return ($user->role_id == 1);
        });

        Gate::define('show-localpost', function ($user, $localpost_group) {
            return ($user->isAdmin()
                || UserRole::getMyAccounts($user->user_id)->contains($localpost_group->account_id));
        });

        Gate::define('allow-localpost', function ($user) {
            $role_id = UserRole::getUserRole($user->user_id)->first()->role_id ?? 0;
            $allow = config('const.ACCESS_CONTROL')[$role_id]['allow_localpost'] ?? 0;
            return ($allow == 1);
        });

        Gate::define('edit-localpost', function ($user) {
            $role_id = UserRole::getUserRole($user->user_id)->first()->role_id ?? 0;
            $allow = config('const.ACCESS_CONTROL')[$role_id]['edit_localpost'] ?? 0;
            return ($allow == 1);
        });

        Gate::define('allow-review', function ($user) {
            $role_id = UserRole::getUserRole($user->user_id)->first()->role_id ?? 0;
            $allow = config('const.ACCESS_CONTROL')[$role_id]['allow_review'] ?? 0;
            return ($allow == 1);
        });

        Gate::define('edit-review', function ($user) {
            $role_id = UserRole::getUserRole($user->user_id)->first()->role_id ?? 0;
            $allow = config('const.ACCESS_CONTROL')[$role_id]['edit_review'] ?? 0;
            return ($allow == 1);
        });
    }
}
