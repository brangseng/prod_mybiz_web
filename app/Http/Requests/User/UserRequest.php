<?php

namespace App\Http\Requests\User;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $pass_rule = [];

        if ($request->method() === 'PUT' || $request->method() === 'PATCH') {
            // 修正時のパスワードルール
            $pass_rule = [
                'min:8',
                'confirmed',
                'regex:/(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)(?=.*?[!-\/:-@[-`{-~])[!-~]{8,16}+/'
            ];
        } else {
            // 新規登録時のパスワードルール
            $pass_rule = [
                'required',
                'min:8',
                'confirmed',
                'regex:/(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)(?=.*?[!-\/:-@[-`{-~])[!-~]{8,16}+/'
            ];
        }

        return [
            'users_name' => 'required|max:255',
            'users_email' => ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore($request->user_id, 'user_id')],
            'users_password' => $pass_rule,
            'enterprise_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'users_password.regex' => "バスワードには英小文字、英大文字、数字、記号それぞれ1文字以上を含めてください。",
        ];
    }

    public function attributes()
    {
        return [
            'users_name' => 'ユーザー名',
            'users_email' => 'emailアドレス',
            'users_password' => 'パスワード',
            'enterprise_id' => '所属企業',
        ];
    }
}
