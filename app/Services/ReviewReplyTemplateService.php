<?php

namespace App\Services;

use App\Account;
use App\ReviewReplyTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReviewReplyTemplateService
{
    public function find($reviewId)
    {
        return ReviewReplyTemplate::findOrFail($reviewId);
    }

    public function search(Request $request)
    {
        $templates = $this->getCondition($request);

        saveMyBrandToSessionCookie($request->account);

        return $templates;
    }

    public function findAccounts()
    {
        $accounts = $this->getAccounts()
            ->select('account_id', 'gmb_account_name')
            ->pluck('gmb_account_name', 'account_id')
            ->all();

        return $accounts;
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $params = array_filter($request->all(), "strlen");
            ReviewReplyTemplate::create($params);
            DB::commit();
            saveMyBrandToSessionCookie($request->account_id);
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            ReviewReplyTemplate::findOrFail($request->input('review_reply_template_id'))
                ->fill($request->all())
                ->save();
            DB::commit();
            saveMyBrandToSessionCookie($request->account_id);
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            ReviewReplyTemplate::findOrFail($request->input('review_reply_template_id'))
                ->fill(['is_deleted' => config('const.FLG_ON')])
                ->save();
            DB::commit();
            saveMyBrandToSessionCookie($request->account);
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    private function getCondition(Request $request)
    {
        $accounts = $this->getAccounts()
            ->select('account_id')
            ->pluck('account_id')
            ->all();

        $templates = ReviewReplyTemplate::active()
            ->whereIn('account_id', $accounts)
            ->where('account_id', $request->input('account'));

        if ($request->filled('target_star_rating')) {
            $templates = $templates->byTargetStarRating($request->input('target_star_rating'));
        }

        if ($request->filled('is_autoreply_template')) {
            switch ($request->input('is_autoreply_template')) {
                case config('const.FLG_OFF'):
                    $templates = $templates->manualReply();
                    break;
                case config('const.FLG_ON'):
                    $templates = $templates->autoReply();
                    break;
            }
        }

        return $templates;
    }

    private function getAccounts()
    {
        $accounts = Account::getMyAccounts(Auth::id());

        return $accounts;
    }
}
