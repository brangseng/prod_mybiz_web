<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Firefox\FirefoxPreferences;
use Facebook\WebDriver\Firefox\FirefoxProfile;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Support\Facades\Storage;

class LocalPostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gmb:localpost';
    protected $driver = null;
    protected $wait_sec = 15;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ChromeDriverを経由して複数店舗への投稿を行います';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->initDriverChrome();
        // $this->initDriverFirefox();
        $this->info('start');
        // $this->login();
        for ($i = 0; $i < 50; $i++) {
            $this->postLocalPosts();
        }
        $this->info('end');
    }

    private function initDriverChrome()
    {
        $host = 'http://localhost:4444/';
        $option = new ChromeOptions();
        $option->addArguments([
            // '--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
            '--window-size=1280,768',
            '--user-data-dir=D:\\temp\\PW\\chrome\\profile',
            // '--user-data-dir=C:/Users/shun/AppData/Local/Google/Chrome/User Data',
            '--profile-directory=Default',
            //'--no-sandbox',
            ]);
        $option->setExperimentalOption('w3c', false);
        $capabilities = DesiredCapabilities::chrome();
        // $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $option);   // depricatedは消えるがoption設定が効かない
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $option);
        $this->driver = RemoteWebDriver::create($host, $capabilities);
        $this->driver->manage()->timeouts()->implicitlyWait(15);
    }

    private function initDriverFirefox()
    {
        $host = 'http://localhost:4444/';
        $prof = new FirefoxProfile();

        $capabilities = DesiredCapabilities::firefox();
        $capabilities->setCapability(FirefoxDriver::PROFILE, $prof);
        $capabilities->setCapability('moz:firefoxOptions', ['binary' => 'D:\\home\\shun\\OneDrive\\CloudSync\\APPS\\FirefoxPortable\\FirefoxPortable.exe']);
        $capabilities->setCapability('platform', 'windows');
        // $capabilities->setCapability('moz:firefoxOptions', ['window-size' => '1024,368']);
        $this->driver = RemoteWebDriver::create($host, $capabilities);
    }

    private function getElm(string $xpath)
    {
        try {
            // 複数取れる前提で、最初にvisibleなelementを対象とする
            $elements = $this->driver->findElements(WebDriverBy::xpath($xpath));
            $target_element = null;
            foreach ($elements as $element) {
                try {
                    // 表示されるようにスクロールする
                    $element->getLocationOnScreenOnceScrolledIntoView();
                    $this->driver->wait(3, 500)->until(
                            WebDriverExpectedCondition::visibilityOf($element)
                        );
                    $target_element = $element;
                } catch (Exception $ex) {
                    $this->info('Exception: ' . $xpath . PHP_EOL . $ex->getMessage());
                }
            }
            return $target_element;
        } catch (Exception $e) {
            $this->info('getElm() Error:' . PHP_EOL . $xpath . PHP_EOL . $e->getMessage());
            // $this->takeScreenshot();
            throw $e;
        }
    }

    private function waitAndClick(string $xpath)
    {
        try {
            $element = $this->getElm($xpath);   // getElm() 内でスクロール済み
            // $this->driver->wait($this->wait_sec, 500)->until(
            //     WebDriverExpectedCondition::elementToBeClickable(WebDriverBy::xpath($xpath))
            // );
            $element->click();
        } catch (Exception $e) {
            $this->info('waitAndClick() Error:' . PHP_EOL . $xpath . PHP_EOL . $e->getMessage());
            // $this->takeScreenshot();
            throw $e;
        }
    }

    private function takeScreenshot()
    {
        $this->driver->takeScreenshot(storage_path() . DIRECTORY_SEPARATOR . time() . ".png");
    }

    private function login()
    {
        // ダッシュボードを開いてみる
        $this->driver->get('https://business.google.com/');
        // 「設定」リンクがあるか？
        $isLogin = $this->getElm('//a[@href="/settings"]');
        // ログイン済みなら戻る
        if ($isLogin) {
            return;
        }
        // ログイン処理
        // ハンバーガーメニューが表示されているか？
        $xpath_humberger_menu_btn = "//div[contains(@class,'h-c-header__hamburger h-c-header__hamburger--first-tier')]";
        $elm_humberger_menu_btn = $this->getElm($xpath_humberger_menu_btn);
        if ($elm_humberger_menu_btn->isDisplayed()) {
            /// ハンバーガーメニューを開く
            $this->waitAndClick($xpath_humberger_menu_btn);
            // ログインボタンを押す
            $this->waitAndClick('//div[contains(@class,"h-c-header__drawer-cta")]//span[text()="ログイン"]');
        } else {
            // ログインボタンを押す
            $this->waitAndClick('//div[contains(@class,"h-c-header__cta")]//span[text()="ログイン"]');
        }

        // ID
        // $this->driver->findElement(WebDriverBy::xpath('//*[@id="identifierId"]'))->click();
        // $this->driver->getKeyboard()->sendKeys('s.free.engineer@gmail.com');
        // $this->driver->findElement(WebDriverBy::xpath('//*[@id="identifierNext"]'))->click();

        // $this->driver->quit();
    }

    private function postLocalPosts()
    {
        // 投稿ボタンクリック
        $this->waitAndClick('//div[@role="menu"]//a[@title="投稿"]//span[text()="投稿"]');
        sleep(1.5);
        // 「最新情報を追加」
        $this->waitAndClick('//div[text()="最新情報を追加"]');
        //$this->waitAndClick('//div[2]/div[1]/c-wiz/div[1]/div/div[2]/span/div/div[7]/div');

        // 文章入力テキストエリアをクリックする
        $this->waitAndClick('//section//*[text()="投稿を入力"]/../../../textarea');
        sleep(0.5);
        $this->driver->getKeyboard()->sendKeys("投稿テスト：" . time());
        sleep(1.5);

        // 公開ボタンをクリックする
        // $this->driver->getKeyboard()->sendKeys(WebDriverKeys::TAB);
        // sleep(0.5);
        // $this->driver->getKeyboard()->sendKeys(WebDriverKeys::TAB);
        // sleep(0.5);
        // $this->driver->getKeyboard()->sendKeys(WebDriverKeys::TAB);
        // sleep(0.5)
        // $this->driver->getKeyboard()->sendKeys(WebDriverKeys::TAB);
        // sleep(1.5);
        // $this->driver->getKeyboard()->sendKeys(WebDriverKeys::RETURN_KEY);
        $this->waitAndClick('//section//button//*[text()="公開"]/..');
    }
}
