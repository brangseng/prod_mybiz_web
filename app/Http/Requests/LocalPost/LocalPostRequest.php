<?php

namespace App\Http\Requests\LocalPost;

use App\Enums\SyncStatusType;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class LocalPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_id' => "required|min:1",
            'account_id' => "required",
            'gmb_event_title' => [
                'max:58',
                'required_if:gmb_topic_type,"EVENT","OFFER"',
            ],
            'gmb_summary' => [
                'required',
                'max:1500'
            ],
            'gmb_event_start_time' => [
                'required_if:gmb_topic_type,"EVENT","OFFER"'
            ],
            'gmb_event_end_time' => [
                'required_if:gmb_topic_type,"EVENT","OFFER"'
            ],
            'upload_files.*' => [
                'dimensions:min_width=400,min_height=300,max_width=10000,max_height=10000',
                'mimes:jpg,png,jpeg',
                'max:81920',
            ],
        ];
    }

    /**
     * バリーデーションのためにデータを準備
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $requestType = $this->input('request_type');

        if (config('const.LOCAL_POST_REQUEST_TYPE.DRAFT') == $requestType) {
            $this->merge([
                'is_scheduled' => 0,
                'scheduled_sync_time' => NULL,
                'scheduled_range' => NULL
            ]);
        } else if (config('const.LOCAL_POST_REQUEST_TYPE.SHORTEST_POST') == $requestType) {
            $getNextSyncDateTime = getNextSyncDateTime();
            $this->merge([
                'is_scheduled' => 1,
                'scheduled_sync_time' => $getNextSyncDateTime->format('Y-m-d'),
                'scheduled_range' => $getNextSyncDateTime->format('H:i')
            ]);
        } else if (config('const.LOCAL_POST_REQUEST_TYPE.RESERVATION_POST') == $requestType) {
            $this->merge([
                'is_scheduled' => 1
            ]);
        }

    }

    public function withValidator($validator)
    {
        // クーポンURL
        $validator->sometimes('gmb_offer_redeem_online_url', 'url', function ($input) {
            return $input->gmb_topic_type === "OFFER";
        });

        // クーポンコード
        $validator->sometimes('gmb_offer_coupon_code', 'max:58', function ($input) {
            return $input->gmb_topic_type === "OFFER";
        });

        // クーポン利用規約
        $validator->sometimes('gmb_offer_terms_conditions', 'max:5000', function ($input) {
            return $input->gmb_topic_type === "OFFER";
        });

        // ボタンの追加
        $validator->sometimes('gmb_action_type_url', 'required|url', function ($input) {
            return ($input->gmb_topic_type === "STANDARD" || $input->gmb_topic_type === "EVENT")
                && ($input->gmb_action_type !== "ACTION_TYPE_UNSPECIFIED" && $input->gmb_action_type !== "CALL");
        });

        // イベント開始日
        $validator->sometimes('gmb_event_start_time', 'date_format:Y-m-d', function ($input) {
            return ($input->gmb_topic_type === "OFFER" || $input->gmb_topic_type === "EVENT")
                && $input->gmb_has_event_time == 0;
        });
        $validator->sometimes('gmb_event_start_time', 'date_format:Y-m-d H:i', function ($input) {
            return ($input->gmb_topic_type === "OFFER" || $input->gmb_topic_type === "EVENT")
                && $input->gmb_has_event_time == 1;
        });

        // イベント終了日
        $validator->sometimes('gmb_event_end_time', 'date_format:Y-m-d', function ($input) {
            return ($input->gmb_topic_type === "OFFER" || $input->gmb_topic_type === "EVENT")
                && $input->gmb_has_event_time == 0;
        });
        $validator->sometimes('gmb_event_end_time', 'date_format:Y-m-d H:i', function ($input) {
            return ($input->gmb_topic_type === "OFFER" || $input->gmb_topic_type === "EVENT")
                && $input->gmb_has_event_time == 1;
        });


        $validator->after(function ($validator) {
            // イベンチの開始日/終了日のチェック
            if (($this->gmb_event_start_time && $this->gmb_event_end_time) &&
                ($this->gmb_topic_type == 'EVENT' || $this->gmb_topic_type == 'OFFER')) {
                if ($this->gmb_event_start_time >= $this->gmb_event_end_time) {
                    $validator->errors()->add('gmb_event_start_time', '開始日には終了日よりも前の日時を指定してください');
                }
                if (Carbon::parse($this->gmb_event_end_time) <= Carbon::now()) {
                    $validator->errors()->add('gmb_event_end_time', '終了日には未来の日時を指定してください');
                }
            }

            // 予約するがチェック入っている かつ 投稿日と投稿日時が設定されている
            if ($this->is_scheduled && $this->scheduled_sync_time && $this->scheduled_range) {
                if (!strtotime($this->scheduled_sync_time)) {
                    $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 の日付形式が不正です。YYYY-MM-DD形式で入力、または、カレンダーから選択してください。');
                } else {
                    if (Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') < Carbon::now()->format('Y-m-d')) {
                        $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 には現在の日付以降を入力、または、カレンダーから選択してください。');
                    }
                    if ((Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') == Carbon::now()->format('Y-m-d')) && $this->scheduled_range) {
                        if (Carbon::parse($this->scheduled_range)->format('H:i:s') < Carbon::now()->format('H:i:s')) {
                            $validator->errors()->add('scheduled_range', '投稿日時の時間 には現在の時間以降を選択してください。');
                        }
                    }
                }
            } elseif ($this->is_scheduled) {
                if (!$this->scheduled_sync_time) {
                    $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 は必須です。または、カレンダーから選択してください。');
                }
                if (!$this->scheduled_range) {
                    $validator->errors()->add('scheduled_range', '投稿日時の時間 は必須です。または、カレンダーから選択してください。');
                }
            }
        });
    }

    public function messages()
    {
        return [
            'gmb_event_title.required_if' => "イベント／特典の場合はタイトルを入力してください",
            'gmb_action_type_url.required_unless' => 'ボタンを追加する場合はURLを指定してください',
            'gmb_event_start_time.required_if' => "イベント／特典の場合は :attribute を指定してください",
            'gmb_event_end_time.required_if' => "イベント／特典の場合は :attribute を指定してください",
        ];
    }

    public function attributes()
    {
        return [
            'location_id' => '店舗',
            'account_id' => 'ブランド',
            'gmb_summary' => '詳細',
            'gmb_action_type' => 'アクションタイプ',
            'gmb_action_type_url' => 'アクションタイプURL',
            'gmb_event_title' => 'イベントタイトル',
            'gmb_event_start_time' => '開始日時',
            'gmb_event_end_time' => '終了日時',
            'gmb_search_url' => 'URL',
            'gmb_topic_type' => '投稿タイプ',
            'gmb_offer_coupon_code' => 'クーポンコード',
            'gmb_offer_redeem_online_url' => 'クーポンURL',
            'gmb_offer_terms_conditions' => '利用規約',
            // media fields
            'gmb_source_url' => '写真・動画URL',
            'upload_files.*' => '写真',
        ];
    }
}
