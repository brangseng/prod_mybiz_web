<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static DRAFT()
 * @method static static QUEUED()
 * @method static static FAILED()
 * @method static static CANCEL()
 * @method static static SYNCED()
 */
final class SyncStatusType extends Enum
{
    const DRAFT = 1;
    const QUEUED = 2;
    const FAILED = 3;
    const CANCEL = 4;
    const SYNCED = 5;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value):string
    {
        switch ($value){
            case self::DRAFT:
                return 'DRAFT';
                brake;
            case self::QUEUED:
                return 'QUEUED';
                brake;
            case self::FAILED:
                return 'FAILED';
                brake;
            case self::CANCEL:
                return 'CANCEL';
                brake;
            case self::SYNCED:
                return 'SYNCED';
                brake;
            default:
                return self::getKey($value);
        }
    }

    public static function getValue(string $key)
    {
        switch ($key){
            case 'DRAFT':
                return 1;
            case 'QUEUED':
                return 2;
            case 'FAILED':
                return 3;
            case 'CANCEL':
                return 4;
            case 'SYNCED':
                return 5;
            default:
                return self::getKey($key);
        }
    }

    public static function getString($value):string
    {
        switch ($value){
            case 'DRAFT':
                return '下書き';
                brake;
            case 'QUEUED':
                return '同期待ち';
                brake;
            case 'FAILED':
                return '同期失敗';
                brake;
            case 'CANCEL':
                return '一部同期失敗';
                brake;
            case 'SYNCED':
                return '同期済';
                brake;
            default:
                return self::getKey($value);
        }
    }

}
