<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static STAR_RATING_UNSPECIFIED()
 * @method static static ONE()
 * @method static static TWO()
 * @method static static THREE()
 * @method static static FOUR()
 * @method static static FIVE()
 */
final class StarRatingType extends Enum
{
    const STAR_RATING_UNSPECIFIED = 0;
    const ONE = 1;
    const TWO = 2;
    const THREE = 3;
    const FOUR = 4;
    const FIVE = 5;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value):string
    {
        switch ($value){
            case self::STAR_RATING_UNSPECIFIED:
                return 'STAR_RATING_UNSPECIFIED';
                brake;
            case self::ONE:
                return 'ONE';
                brake;
            case self::TWO:
                return 'TWO';
                brake;
            case self::THREE:
                return 'THREE';
                brake;
            case self::FOUR:
                return 'FOUR';
                brake;
            case self::FIVE:
                return 'FIVE';
                brake;
            default:
                return self::getKey($value);
        }
    }

    public static function getValue(string $key)
    {
        switch ($key){
            case 'STAR_RATING_UNSPECIFIED':
                return 0;
            case 'ONE':
                return 1;
            case 'TWO':
                return 2;
            case 'THREE':
                return 3;
            case 'FOUR':
                return 4;
            case 'FIVE':
                return 5;
            default:
                return self::getValue($key);
        }
    }

}
