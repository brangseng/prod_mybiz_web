<?php

namespace App\Http\Controllers;

use App\Account;
use App\Enterprise;
use App\Http\Requests\User\UserRequest;
use App\Services\UserService;
use App\User;
use App\UserRole;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    // ユーザー登録を行うため trait を利用する
    use RegistersUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserService $service)
    {
        Gate::authorize('admin');

        $users = $service->search($request)->paginate(config('formConst.PAGINATE_MAX_LIMIT'));
        $enterprises = $service->getEnterprises()->all();
        return view('user/index', compact('users', 'enterprises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        Gate::authorize('admin');

        $variables = [
            'user' => new User(),
            'isCreate' => true,
            'isReadOnly' => false,
            'enterprises' => Enterprise::all(),
            'accounts' => Account::all(),
            'user_roles' => null
        ];
        return view('user/create', $variables);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request, UserService $service)
    {
        try {
            DB::beginTransaction();
            $new_user = $this->create($request->all());
            event(new Registered($new_user));
            $service->saveUserRoles($request, $new_user);
            DB::commit();
            return redirect(route('user.index'))->with('success', 'ユーザーを登録しました');
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error($e);
            return back()->withErrors('ユーザーの登録時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, UserService $service)
    {
        Gate::authorize('admin');

        $params = [
            'user' => $user,
            'isCreate' => false,
            'isReadOnly' => false,
            'enterprises' => Enterprise::all(),
            'accounts' => Account::all(),
            'user_roles' => UserRole::getUserRole($user->user_id)->get(),
            'access_control' => config('const.ACCESS_CONTROL')[UserRole::getUserRole($user->user_id)->first()->role_id ?? 0]
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('user/create', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user, UserService $service)
    {
        try {
            DB::beginTransaction();
            $user = $this->create($request->all());
            $service->saveUserRoles($request, $user);
            event(new Registered($user));
            DB::commit();
            return redirect(route('user.index'))->with('success', 'ユーザーを更新しました');
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error($e);
            return back()->withErrors('ユーザーの更新時にエラーが発生しました')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UserService $service)
    {
        try {
            $service->destroy($request);
            return redirect(action('UserController@index'))->with('success', 'ユーザーを削除しました');
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            return back()->withErrors('ユーザーの削除時にエラーが発生しました')->withInput();
        }
    }

    // php artisan make:auth で生成される RegisterController.php からオーバーライド用にコピー＋修正した。
    // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data): User
    {
        $current_user = Auth::user();
        // role_id の値は固定。所属企業がParaWorksさんかどうか？で判定する。
        $users_role_id
            = ($data['enterprise_id'] == config('formConst.PARAWORKS_ENTERPRISE_ID')) ? 1 : 0;
        $values = [
            'name' => $data['users_name'],
            'email' => $data['users_email'],
            'role_id' => $users_role_id,
            'enterprise_id' => $data['enterprise_id'],
        ];
        // 新規登録、編集でpassword指定があった場合
        if ($data['users_password']) {
            $values['password'] = bcrypt($data['users_password']);
        }

        return User::updateOrCreate(
            [
                'user_id' => $data['user_id']
            ],
            $values
        );
    }
    // ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲

}
