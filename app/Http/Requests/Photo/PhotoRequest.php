<?php

namespace App\Http\Requests\Photo;

use App\Enums\SyncStatusType;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_id' => "required|min:1",
            'account_id' => "required",
            'gmb_location_association_category' => "required",
            'upload_files.*' => [
//                'dimensions:min_width=400,min_height=300,max_width=1000000,max_height=1000000',
                'mimes:jpg,png,jpeg,mp4,qt,x-ms-wmv,mpeg,x-msvideo',
                'max:83886080',
            ],
        ];
    }

    /**
     * バリーデーションのためにデータを準備
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $requestType = $this->input('request_type');

        if (config('const.LOCAL_POST_REQUEST_TYPE.DRAFT') == $requestType) {
            $this->merge([
                'is_scheduled' => 0,
                'scheduled_sync_time' => NULL,
                'scheduled_range' => NULL
            ]);
        } else if (config('const.LOCAL_POST_REQUEST_TYPE.SHORTEST_POST') == $requestType) {
            $getNextSyncDateTime = getNextSyncDateTime();
            $this->merge([
                'is_scheduled' => 1,
                'scheduled_sync_time' => $getNextSyncDateTime->format('Y-m-d'),
                'scheduled_range' => $getNextSyncDateTime->format('H:i')
            ]);
        } else if (config('const.LOCAL_POST_REQUEST_TYPE.RESERVATION_POST') == $requestType) {
            $this->merge([
                'is_scheduled' => 1
            ]);
        }

    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            // ファイル数のバリデーションチェック
            if ($this->media_item2_group_id == "") {
                if(is_null($this->upload_files)){
                    $validator->errors()->add('upload_files', 'ファイルが選択されていません');
                } elseif (!is_null($this->upload_files) && count($this->upload_files) > config('const.MAXIMUM_OF_FILES')){
                    $validator->errors()->add('upload_files', 'ファイルは10個までしか選択できません。');
                }
            } else {
                if ((is_null($this->upload_files) && $this->all_delete_media_item2 == "true")) {
                    $validator->errors()->add('upload_files', 'ファイルが選択されていません');
                } else if (!is_null($this->upload_files) && count($this->upload_files) > config('const.MAXIMUM_OF_FILES')) {
                    $validator->errors()->add('upload_files', 'ファイルは10個までしか選択できません。');
                }
            }

            // 予約するがチェック入っている かつ 投稿日と投稿日時が設定されている
            if ($this->is_scheduled && $this->scheduled_sync_time && $this->scheduled_range) {
                if (!strtotime($this->scheduled_sync_time)) {
                    $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 の日付形式が不正です。YYYY-MM-DD形式で入力、または、カレンダーから選択してください。');
                } else {
                    if (Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') < Carbon::now()->format('Y-m-d')) {
                        $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 には現在の日付以降を入力、または、カレンダーから選択してください。');
                    }
                    if ((Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') == Carbon::now()->format('Y-m-d')) && $this->scheduled_range) {
                        if (Carbon::parse($this->scheduled_range)->format('H:i:s') < Carbon::now()->format('H:i:s')) {
                            $validator->errors()->add('scheduled_range', '投稿日時の時間 には現在の時間以降を選択してください。');
                        }
                    }
                }
            } elseif ($this->is_scheduled) {
                if (!$this->scheduled_sync_time) {
                    $validator->errors()->add('scheduled_sync_time', '投稿日時の投稿日 は必須です。または、カレンダーから選択してください。');
                }
                if (!$this->scheduled_range) {
                    $validator->errors()->add('scheduled_range', '投稿日時の時間 は必須です。または、カレンダーから選択してください。');
                }
            }
        });
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [
            'location_id' => '店舗',
            'account_id' => 'ブランド',
            'upload_files.*' => '写真・動画',
            'gmb_location_association_category' => 'カテゴリ'
        ];
    }
}
