<?php

namespace App\Services;

use App\Enterprise;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService
{
    public function search(Request $request)
    {
        $users = $request->enterprise
            ? User::where('enterprise_id', '=', $request->enterprise)
            : User::query();
        return $users;
    }

    public function getEnterprises()
    {
        $enterprises = Enterprise::select('enterprise_id', 'name')
            ->pluck('name', 'enterprise_id');
        return $enterprises;
    }

    public function saveUserRoles(Request $request, User $user)
    {
        // 一旦全削除する
        UserRole::where('user_id', '=', $user->user_id)->delete();

        $allow_localpost = $request->input('allow_local_post') ? 1 : 0;
        if ($allow_localpost == 0) {
            $edit_localpost = 0;
        } else {
            $edit_localpost = $request->input('edit_local_post') ? 1 : 0;
        }
        $allow_review = $request->input('allow_review') ? 1 : 0;
        if ($allow_review == 0) {
            $edit_review = 0;
        } else {
            $edit_review = $request->input('edit_review') ? 1 : 0;
        }

        $access_control = array(
            'allow_localpost' => $allow_localpost,
            'edit_localpost'  => $edit_localpost,
            'allow_review'    => $allow_review,
            'edit_review'     => $edit_review
        );

        $user_roles_role_id = array_search($access_control, config('const.ACCESS_CONTROL'));

        if ($user->role_id == '0') {
            if ($user_roles_role_id == 0) {
                $user_roles_role_id = 18;
            } elseif ($user_roles_role_id == 1) {
                $user_roles_role_id = 10;
            }
        }

        foreach (($request->account_ids ?? []) as $account_id) {
            UserRole::create([
                'user_id' => $user->user_id,
                'account_id' => $account_id,
                'location_id' => 0,
                'role_id' => $user_roles_role_id,
                'create_user_id' => Auth::id(),
                'update_user_id' => Auth::id(),
            ]);
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            UserRole::where('user_id', '=', $request->input('destroy_user_id'))
                ->delete();
            User::findOrFail($request->input('destroy_user_id'))
                ->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
