<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\LocalPost\LocalPostRequest;
use App\Http\Requests\Photo\PhotoRequest;
use App\LocalPost;
use App\LocalPostGroup;
use App\Location;
use App\MediaItem2Group;
use App\Services\LocalPostGroupService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Services\PhotoService;

class PhotoController extends Controller
{
    public function index(Request $request, PhotoService $service)
    {
        if (is_null($request->input('account'))) {
            if (session(config('formConst.SESSION_MY_BRAND'))) {
                $account_id = session(config('formConst.SESSION_MY_BRAND'));
            } else {
                $account = Account::getMyAccounts(Auth::id())->select('account_id')->first();
                if ($account) {
                    $account_id = $account->account_id;
                } else {
                    $account_id = null;
                }
            }
            $request->merge(['account' => $account_id]);
        }
        if (is_null($request->input('period'))) {
            if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
                $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
            } else {
                $contract_type = Auth::user()->getContractType() ?? 1;
            }
            $request->merge(['period' => config('const.CONTRACT_TYPE_PERIOD')[$contract_type]]);
        }
        $accounts = $service->findAccounts();
        $photos   = $service->search($request);
        return view('photo/index', compact('accounts', 'photos'));
    }

    public function create()
    {
        $variables = [
            'media_item2_group' => new MediaItem2Group(),
            'mediaItem2' => [],
            'isCreate' => true,
            'isReadOnly' => false,
            'locations' => Location::getMyLocations(Auth::id())->get(),
            'accounts' => Account::getMyAccounts(Auth::id())->get(),
            'prefs' => config('pref'),
            'maxFileLength' => config('const.MAXIMUM_OF_FILES')
        ];
        return view('photo/create', $variables);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $mediaItem2GroupId
     * @return \Illuminate\Http\Response
     */
    public function edit(int $mediaItem2GroupId)
    {
        $mediaItem2Group = MediaItem2Group::findOrFail($mediaItem2GroupId);

        if (Gate::denies('show-localpost', $mediaItem2Group)) {
            abort(403, '権限がありません');
        }

        if (!$mediaItem2Group) {
            return App::abort(404);
        }

        $params = [
            'media_item2_group' => $mediaItem2Group,
            'mediaItem2' => $mediaItem2Group->getFileList(),
            'isCreate' => false,
            'isReadOnly' => !$mediaItem2Group->isEditable(),
            'locations' => Location::getMyLocations(Auth::id())->get(),
            'accounts' => Account::getMyAccounts(Auth::id())->get(),
            'prefs' => config('pref'),
            'maxFileLength' => config('const.MAXIMUM_OF_FILES')
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('photo/create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PhotoRequest  $request
     * @param  PhotoService  $service
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoRequest $request, PhotoService $service)
    {
        try {
            $media_item2_group = $service->store($request);
            return redirect(action(
                "PhotoController@edit",
                ['mediaItem2GroupId' => $media_item2_group->media_item2_group_id]
            ))->with('success', '写真・動画を保存しました');
        } catch (Exception $e) {
            report($e);
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    public function update(PhotoRequest $request, PhotoService $service)
    {
        try {
            $service->update($request);
            return redirect(action(
                "PhotoController@edit",
                ['mediaItem2GroupId' => $request->input('media_item2_group_id')]
            ))->with('success', '写真・動画を保存しました');
        } catch (Exception $e) {
            logger()->error($e);
            return back()->withErrors('データの保存時にエラーが発生しました')->withInput();
        }
    }

    public function delete(Request $request, PhotoService $service)
    {
        try {
            $service->delete($request);
            return redirect(action('PhotoController@index'))->with('success', '写真を削除しました');
        } catch (Exception $e) {
            return back()->withErrors('写真の削除時にエラーが発生しました');
        }
    }
}
