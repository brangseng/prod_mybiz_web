<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $dates = ['create_time', 'update_time'];

    protected $primaryKey = 'location_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'gmb_account_id',
        'gmb_location_id',
        'gmb_language_code',
        'gmb_store_code',
        'gmb_location_name',
        'gmb_primary_phone',
        'gmb_additional_phones_1',
        'gmb_additional_phones_2',
        'gmb_postaladdr_region_code',
        'gmb_postaladdr_language_code',
        'gmb_postaladdr_postal_code',
        'gmb_postaladdr_sorting_code',
        'gmb_postaladdr_admin_area',
        'gmb_postaladdr_locality',
        'gmb_postaladdr_sublocality',
        'gmb_postaladdr_address_lines',
        'gmb_postaladdr_recipients',
        'gmb_postaladdr_organization',
        'gmb_primary_category_id',
        'gmb_website_url',
        'gmb_servicearea_business_type',
        'gmb_servicearea_latitude',
        'gmb_servicearea_longitude',
        'gmb_servicearea_radius_km',
        'gmb_servicearea_placeinfo',
        'gmb_locationkey_pluspage_id',
        'gmb_locationkey_place_id',
        'gmb_locationkey_explicit_no_place_id',
        'gmb_locationkey_request_id',
        'gmb_labels',
        'gmb_adwords_adphone',
        'gmb_latlng_latitude',
        'gmb_latlng_longitude',
        'gmb_openinfo_status',
        'gmb_openinfo_can_reopen',
        'gmb_openinfo_opening_date',
        'gmb_state_is_google_updated',
        'gmb_state_is_duplicate',
        'gmb_state_is_suspended',
        'gmb_state_can_update',
        'gmb_state_can_delete',
        'gmb_state_is_verified',
        'gmb_state_needs_reverification',
        'gmb_state_is_pending_review',
        'gmb_state_is_disabled',
        'gmb_state_is_published',
        'gmb_state_is_disconnected',
        'gmb_state_is_local_post_api_disabled',
        'gmb_state_has_pending_edits',
        'gmb_state_has_pending_verification',
        'gmb_metadata_duplicate_location_name',
        'gmb_metadata_duplicate_place_id',
        'gmb_metadata_duplicate_access',
        'gmb_metadata_maps_url',
        'gmb_metadata_new_review_url',
        'gmb_profile_description',
        'gmb_relationship_parent_chain',
        'review_is_autoreplied',
        'is_deleted',
        'sync_status',
        'scheduled_sync_time',
        'sync_time',
        'update_user_id'
    ];

    protected $attributes = [
        'account_id' => 1,
        'gmb_account_id' => '',
        'gmb_location_id' => '',
        'gmb_postaladdr_region_code' => '',
        'review_is_autoreplied' => 0,
        'is_deleted' => 0
    ];

    public function localPosts()
    {
        return $this->hasMany('App\LocalPost', 'location_id', 'location_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'account_id');
    }

    public function attributes()
    {
        return $this->belongsToMany('App\Attribute', 'location_attributes', 'location_id', 'attribute_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'location_categories', 'location_id', 'category_id');
    }

    public function businessHours()
    {
        return $this->hasMany('App\BusinessHour');
    }

    public function specialHours()
    {
        return $this->hasMany('App\SpecialHour');
    }

    // アクセサ

    public function getGmbPrimaryCategoryIdAttribute($value)
    {
        return (int)$value;
    }

    // ミューテタ
    public function setGmbPrimaryCategoryIdAttribute($value)
    {
        $this->attributes['gmb_primary_category_id'] = (int)$value;
    }

    // クエリスコープ
    public function scopeActive($query)
    {
        return $query
            ->where('is_deleted', '=', config('const.FLG_OFF'))
            // user_role 側で location_id に 0 が入っているデータに「ブランド傘下の全店舗に対して権限あり」という
            // 特殊な意味を持たせているが、その際に外部キー制約に引っかかるため、locations 側にも location_id = 0 の
            // データが必要となる。locations.location_id = 0 のレコードはそのためのダミーデータであるため除外する。
            ->where('location_id', '>', 0);
    }

    // ログインユーザーが管理可能な location を返す
    public function scopeGetMyLocations($query, $user_id)
    {
        $accounts = UserRole::getUserRole($user_id)
            ->where('location_id', 0)
            ->select('account_id')
            ->pluck('account_id')
            ->all();

        $account_role_locations = Location::active()
            ->whereIn('account_id', $accounts)
            ->select('location_id')
            ->pluck('location_id')
            ->all();

        $location_role_locations = UserRole::getUserRole($user_id)
            ->where('location_id', '!=', 0)
            ->select('location_id')
            ->pluck('location_id')
            ->all();

        $locations = Location::active()
            ->where(function ($query) use ($account_role_locations, $location_role_locations) {
                $query->whereIn('location_id', $account_role_locations)
                    ->orWhereIn('location_id', $location_role_locations);
            })
            ->orderBy('gmb_location_name');

        return $locations;
    }

    public function scopeByLocations($query, $locations)
    {
        return $query->whereIn('location_id', $locations);
    }
}
