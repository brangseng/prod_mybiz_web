<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $hidden = [];

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $dates   = ['create_time', 'update_time'];

    protected $primaryKey = 'account_id';

    protected $attributes = [
        'is_deleted' => 0,
    ];

    public function scopeActive($query)
    {
        return $query->where('is_deleted', '=', config('const.FLG_OFF'));
    }

    public function locations()
    {
        return $this->hasMany(Location::class, 'account_id', 'account_id');
    }

    public function localPostGroups()
    {
        return $this->hasMany(LocalPostGroup::class, 'account_id', 'account_id');
    }

    public function review_reply_templates()
    {
        return $this->hasMany(ReviewReplyTemplate::class, 'review_reply_template_id');
    }

    public function scopeGetMyAccounts($query, $user_id)
    {
        if (User::find($user_id)->isAdmin()) {
            // 管理者の場合はすべて選択可能
            $query = $query->active()
                ->orderBy('account_id');
        } else {
            $query = $query->active()
                ->whereIn('account_id', UserRole::GetUserRole($user_id)->select('account_id')->pluck('account_id')->all())
                ->orderBy('account_id');
        }
        return $query;
    }
}
