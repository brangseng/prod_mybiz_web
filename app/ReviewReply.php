<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewReply extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $primaryKey = 'review_reply_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'review_id',
        'gmb_comment',
        'is_deleted',
        'sync_type',
        'sync_status',
        'scheduled_sync_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [
        'scheduled_sync_time',
        'create_time',
        'update_time'
    ];

    protected $attributes = [
        'is_deleted' => 0
    ];

    public function scopeActive($query)
    {
        return $query->where('is_deleted', '=', config('const.FLG_OFF'));
    }

    public function scopePreviousReply($query, $reviewId)
    {
        return $query->where('review_id', '=', $reviewId);
    }

    public function scopeByCreateStDate($query, $stDate)
    {
        return $query->where('gmb_create_date', '>=', $stDate);
    }

    public function scopeByCreateEndDate($query, $endDate)
    {
        return $query->where('gmb_create_date', '<=', $endDate);
    }

    public function scopeBetweenCreateDate($query, $stDate, $endDate)
    {
        return $query->whereBetween('gmb_create_date', [$stDate, $endDate]);
    }

    public function scopeByStarRating($query, $rate)
    {
        return $query->where('gmb_star_rating', '=', $rate);
    }
}
