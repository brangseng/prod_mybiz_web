<?php

namespace App\Http\Requests\Template;

use Illuminate\Foundation\Http\FormRequest;
// use Illuminate\Validation\Rule;
// use Illuminate\Http\Request;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_id' => ['required'],
            'template_name'  => ['required','max:100'],
            'template'  => ['required','max:1000'],
            'target_star_rating' => ['required']
            //     account_id ,
            //     Rule::unique('review_reply_templates')
            //         ->ignore($this->input('review_reply_template_id'), 'review_reply_template_id')
            //         ->where(function ($query) {
            //             return $query->where('account_id', $this->account_id)
            //                 ->where('is_deleted', config('const.FLG_OFF'));
            //         }),
            // ]
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // if (Request::input('is_autoreply_template') == config('const.FLG_OFF') &&
            //     Request::filled('target_star_rating')) {
            //     $validator->errors()->add('target_star_rating', '手動返信の場合は選択できません');
            // }
        });
    }

    public function attributes()
    {
        return [
            'account_id' => 'ブランド',
            'template_name'   => 'テンプレート名',
            'template'  => '返信内容',
            'target_star_rating' => '自動返信クチコミ評点'
        ];
    }

    public function messages()
    {
        return [
            // 'target_star_rating.unique' => '同じブランド・同じクチコミ評点のテンプレートが既に存在します。'
        ];
    }
}
