<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static CATEGORY_UNSPECIFIED()
 * @method static static COVER()
 * @method static static PROFILE()
 * @method static static LOGO()
 * @method static static EXTERIOR()
 * @method static static INTERIOR()
 * @method static static PRODUCT()
 * @method static static AT_WORK()
 * @method static static FOOD_AND_DRINK()
 * @method static static MENU()
 * @method static static COMMON_AREA()
 * @method static static ROOMS()
 * @method static static TEAMS()
 * @method static static ADDITIONAL()
 */
final class CategoryType extends Enum
{
    const CATEGORY_UNSPECIFIED = 0;
    const COVER = 1;
    const PROFILE = 2;
    const LOGO = 3;
    const EXTERIOR = 4;
    const INTERIOR = 5;
    const PRODUCT = 6;
    const AT_WORK = 7;
    const FOOD_AND_DRINK = 8;
    const MENU = 9;
    const COMMON_AREA = 10;
    const ROOMS = 11;
    const TEAMS = 12;
    const ADDITIONAL = 13;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value):string
    {
        switch ($value){
            case self::CATEGORY_UNSPECIFIED:
                return 'CATEGORY_UNSPECIFIED';
                brake;
            case self::COVER:
                return 'COVER';
                brake;
            case self::PROFILE:
                return 'PROFILE';
                brake;
            case self::LOGO:
                return 'LOGO';
                brake;
            case self::EXTERIOR:
                return 'EXTERIOR';
                brake;
            case self::INTERIOR:
                return 'INTERIOR';
                brake;
            case self::PRODUCT:
                return 'PRODUCT';
                brake;
            case self::AT_WORK:
                return 'AT_WORK';
                brake;
            case self::FOOD_AND_DRINK:
                return 'FOOD_AND_DRINK';
                brake;
            case self::MENU:
                return 'MENU';
                brake;
            case self::COMMON_AREA:
                return 'COMMON_AREA';
                brake;
            case self::ROOMS:
                return 'ROOMS';
                brake;
            case self::TEAMS:
                return 'TEAMS';
                brake;
            case self::ADDITIONAL:
                return 'ADDITIONAL';
                brake;
            default:
                return self::getKey($value);
        }
    }

    public static function getValue(string $key)
    {
        switch ($key){
            case 'CATEGORY_UNSPECIFIED':
                return 0;
            case 'COVER':
                return 1;
            case 'PROFILE':
                return 2;
            case 'LOGO':
                return 3;
            case 'EXTERIOR':
                return 4;
            case 'INTERIOR':
                return 5;
            case 'PRODUCT':
                return 6;
            case 'AT_WORK':
                return 7;
            case 'FOOD_AND_DRINK':
                return 8;
            case 'MENU':
                return 9;
            case 'COMMON_AREA':
                return 10;
            case 'ROOMS':
                return 11;
            case 'TEAMS':
                return 12;
            case 'ADDITIONAL':
                return 13;
            default:
                return self::getValue($key);
        }
    }

}
