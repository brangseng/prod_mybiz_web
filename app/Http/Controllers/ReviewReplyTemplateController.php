<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Template\CreateEditRequest;
use App\Account;
use App\ReviewReplyTemplate;
use App\Services\ReviewReplyTemplateService;
use Exception;

class ReviewReplyTemplateController extends Controller
{
    public function index(Request $request, ReviewReplyTemplateService $service)
    {
        if (is_null($request->input('account'))) {
            if (session(config('formConst.SESSION_MY_BRAND'))) {
                $account_id = session(config('formConst.SESSION_MY_BRAND'));
            } else {
                $account = Account::getMyAccounts(Auth::id())->select('account_id')->first();
                if ($account) {
                    $account_id = $account->account_id;
                } else {
                    $account_id = null;
                }
            }
            $request->merge(['account' => $account_id]);
        }
        $accounts  = $service->findAccounts();
        $templates = $service->search($request)->paginate(config('formConst.PAGINATE_MAX_LIMIT'));
        return view('review_reply_template/index', compact('accounts', 'templates'));
    }

    public function create(ReviewReplyTemplateService $service)
    {
        $params = [
            'isCreate' => true,
            'template' => new ReviewReplyTemplate(),
            'accounts' => $service->findAccounts()
        ];
        return view('review_reply_template/create', $params);
    }

    public function edit($reviewReplyTemplateId, ReviewReplyTemplateService $service)
    {
        $params = [
            'isCreate' => false,
            'template' => $service->find($reviewReplyTemplateId),
            'accounts' => $service->findAccounts()
        ];
        // 新規登録・更新を同じ blade で行っているため create を呼び出す
        return view('review_reply_template/create', $params);
    }

    public function store(CreateEditRequest $request, ReviewReplyTemplateService $service)
    {
        try {
            $service->store($request);
            return redirect(action('ReviewReplyTemplateController@index'))->with('success', '返信テンプレートを保存しました');
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            return back()->withErrors('返信テンプレートの保存時にエラーが発生しました')->withInput();
        }
    }

    public function update(CreateEditRequest $request, ReviewReplyTemplateService $service)
    {
        try {
            $service->update($request);
            return redirect(action('ReviewReplyTemplateController@index'))->with('success', '返信テンプレートを更新しました');
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            return back()->withErrors('返信テンプレートの更新時にエラーが発生しました')->withInput();
        }
    }

    public function delete(Request $request, ReviewReplyTemplateService $service)
    {
        // ajax の場合
        if ($request->ajax()) {
            return ['result' => $service->delete($request)];
        }

        // ajax 以外
        try {
            $service->delete($request);
            return redirect(action('ReviewReplyTemplateController@index'))->with('success', '返信テンプレートを削除しました');
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            return back()->withErrors('返信テンプレートの削除時にエラーが発生しました')->withInput();
        }
    }

    public function asyncFind(Request $request, ReviewReplyTemplateService $service)
    {
        if ($request->ajax()) {
            return ['result' => $service->find($request->input('review_reply_template_id'))];
        }
    }
}
