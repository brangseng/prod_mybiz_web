<?php

namespace App\Http\Requests\Review;

use App\Enums\SyncStatusType;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gmb_comment' => ['required_if:is_deleted, 0', 'max:1000']
        ];
    }

    /**
     * バリーデーションのためにデータを準備
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        switch ($this->request_type) {
            case config('const.REPLY_REQUEST_TYPE.DRAFT'):
                $this->merge(['is_scheduled' => config('const.FLG_OFF')]);
                $this->merge(['scheduled_sync_time' => null]);
                $this->merge(['scheduled_range' => null]);
                break;

            case config('const.REPLY_REQUEST_TYPE.SHORTEST_REPLY'):
                $getNextSyncDateTime = getNextSyncDateTime(null, 'REPLY');
                $this->merge(['is_scheduled' => config('const.FLG_ON')]);
                $this->merge(['scheduled_sync_time' => $getNextSyncDateTime->format('Y-m-d')]);
                $this->merge(['scheduled_range' => $getNextSyncDateTime->format('H:i')]);
                break;

            case config('const.REPLY_REQUEST_TYPE.RESERVATION_REPLY'):
                $this->merge(['is_scheduled' => config('const.FLG_ON')]);
                break;

            case config('const.REPLY_REQUEST_TYPE.DELETE_REPLY'):
                if ($this->sync_status == SyncStatusType::getDescription(5) && $this->gmb_review_reply_comment) {
                    $this->merge(['is_scheduled' => config('const.FLG_ON')]);
                } else {
                    $this->merge(['is_scheduled' => config('const.FLG_OFF')]);
                }
                break;
        }
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->is_scheduled && $this->scheduled_sync_time && $this->scheduled_range) {
                if (!strtotime($this->scheduled_sync_time)) {
                    $validator->errors()->add('scheduled_sync_time', '返信日時の返信日 の日付形式が不正です。YYYY-MM-DD形式で入力、または、カレンダーから選択してください。');
                } else {
                    if (Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') < Carbon::now()->format('Y-m-d')) {
                        $validator->errors()->add('scheduled_sync_time', '返信日時の返信日 には現在の日付以降を入力、または、カレンダーから選択してください。');
                    }
                    if ((Carbon::parse($this->scheduled_sync_time)->format('Y-m-d') == Carbon::now()->format('Y-m-d')) && $this->scheduled_range) {
                        if (Carbon::parse($this->scheduled_range)->format('H:i:s') < Carbon::now()->format('H:i:s')) {
                            $validator->errors()->add('scheduled_range', '返信日時の時間 には現在の時間以降を選択してください。');
                        }
                    }
                }
            } else {
                if ($this->is_scheduled) {
                    if (!$this->scheduled_sync_time) {
                        $validator->errors()->add('scheduled_sync_time', '返信日時の返信日 は必須です。返信日時の返信日 を入力、または、カレンダーから選択してください。');
                    }
                    if (!$this->scheduled_range) {
                        $validator->errors()->add('scheduled_range', '返信日時の時間 は必須です。返信日時の時間 を選択してください。');
                    }
                }
            }
        });
    }

    public function messages()
    {
        return [
            'gmb_comment.required_if' => ':attributeは必須です。'
        ];
    }

    public function attributes()
    {
        return [
            'gmb_comment' => '返信内容',
            'is_deleted' => '削除フラグ',
            'sync_status' => 'API連携ステータス',
            'scheduled_sync_time' => '返信日時の返信日',
            'scheduled_range' => '返信日時の時間'
        ];
    }
}
