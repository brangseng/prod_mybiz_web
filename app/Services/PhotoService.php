<?php

namespace App\Services;

use App\Enums\CategoryType;
use App\Enums\SyncStatusType;
use App\Enums\SyncType;
use App\Account;
use App\LocalPost;
use App\LocalPostGroup;
use App\Location;
use App\MediaItem2;
use App\MediaItem2Group;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;

class PhotoService
{
    public function findAccounts()
    {
        $accounts = $this->getAccounts()
            ->select('account_id', 'gmb_account_name')
            ->pluck('gmb_account_name', 'account_id')
            ->all();

        return $accounts;
    }

    public function search(Request $request)
    {
        $photos = $this->getCondition($request)
            ->select(config('const.SEARCH_RESULT_ITEMS.PHOTO'))
            ->get();

        if ($request->filled('account')) {
            saveMyBrandToSessionCookie($request->account);
        }

        return $photos;
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            $media_item2_group_ids = explode(',', $request->media_item2_group_ids);
            foreach($media_item2_group_ids as $media_item2_group_id) {
                MediaItem2Group::findOrFail($media_item2_group_id)
                    ->fill([
                        'is_deleted' => config('const.FLG_ON'),
                        'update_user_id' => Auth::id()
                    ])
                    ->save();
                $media_item2_ids = MediaItem2::active()
                    ->where('media_item2_group_id', $media_item2_group_id)
                    ->select('media_item2_id')
                    ->pluck('media_item2_id')
                    ->all();
                foreach($media_item2_ids as $media_item2_id) {
                    $this->setSyncStatusDelete($request, $media_item2_id);
                    $update_column = [
                        'is_deleted' => $request->input('is_deleted'),
                        'sync_type' => $request->input('sync_type'),
                        'sync_status' => $request->input('sync_status'),
                        'scheduled_sync_time' => $request->input('scheduled_sync_time'),
                        'update_user_id' => Auth::id()
                    ];
                    MediaItem2::where('media_item2_id', $media_item2_id)
                        ->update($update_column);
                }
            }
            DB::commit();
            if ($request->filled('account')) {
                saveMyBrandToSessionCookie($request->account);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            return false;
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->setSyncStatus($request); // $request内にsync_status をセットする
            $media_item2_group = $this->updateOrCreatePhotoGroup($request);
            DB::commit();
            saveMyBrandToSessionCookie($request->account_id);
            return $media_item2_group;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function update(Request $request)
    {
        try {
            // 新規登録と修正の処理を共通化した
            return $this->store($request);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateOrCreatePhotoGroup(Request $request)
    {
        try {
            // $request に必要なもの、不要なものなど混ざっているため、寄り分けて整備した配列を用意する
            $filtered_request_array = $this->getFilteredRequestArray($request);
            $filtered_request_array['gmb_account_id'] = Account::findOrFail($request->account_id)->gmb_account_id;
            $filtered_request_array['gmb_language_code'] = 'ja';

            $isUpdate = false;

            // 画像の変更が必要かどうか？
            if ($filtered_request_array['media_item2_group_id']) {
                // 修正
                $filtered_request_array['update_user_id'] = Auth::id();
                $media_item2_group = MediaItem2Group::findOrFail($filtered_request_array['media_item2_group_id']);
                $media_item2_group->fill($filtered_request_array)->save();
                $isUpdate = true;
            } else {
                // 新規登録
                $filtered_request_array['create_user_id'] = Auth::id();
                $filtered_request_array['update_user_id'] = Auth::id();
                $media_item2_group = MediaItem2Group::create($filtered_request_array);
            }
            // media_item2_group_id をセットする
            $filtered_request_array['media_item2_group_id'] = $media_item2_group->media_item2_group_id;

            // 【 画像に関する処理 】
            // 各カラムにセットする値は request から来た値を使いまわすが、
            // request を複製した配列を使用する。
            $media_item_values = $filtered_request_array;

            // 同一の local_post_group_id 内で同じ画像URLを使用するのでURLを決定しておく
            // 複数画像を登録する
            $rollbackPathFiles = [];
            if ($request->file('upload_files')) {
                foreach ($request->file('upload_files') as $file) {
                    // 画像の保存pathを決定する
                    $upload_path = $this->getUploadPath($filtered_request_array);
                    // 画像の保存ファイル名を決定する
                    $upload_filenames = $this->getUploadFilenames(array($file), $upload_path);

                    $media_item_values['gmb_media_format'] = $this->getMediaFormat($upload_filenames[0]);

                    $media_item_values['s3_object_url']
                        = $upload_path . '/' . $upload_filenames[0];
                    $media_item_values['gmb_source_url']
                        = $upload_filenames[0]
                        ? Storage::url($upload_path . '/' . $upload_filenames[0])
                        : null;

                    $rollbackPathFiles[] = $media_item_values['s3_object_url'];

                    foreach ($request->location_id as $location_id) {
                        // 新規登録された media_item2_group の media_item2_group_id を media_items にセットする
                        $media_item_values['media_item2_group_id'] = $media_item2_group->media_item2_group_id;
                        // media_items のフィールドのうち、店舗ごとに変わるものもこのループ内でセットする
                        $media_item_values['location_id'] = $location_id;
                        $media_item_values['gmb_location_id'] = Location::findOrFail($location_id)->gmb_location_id;

                        $media_item2_group->mediaItems()->updateOrCreate(
                            [
                                'gmb_location_id' => $media_item_values['gmb_location_id'],
                                'media_item2_group_id' => $media_item_values['media_item2_group_id'],
                                's3_object_url' => $media_item_values['s3_object_url'],
                            ],
                            $media_item_values
                        );

                        // 画像ファイルをS3にアップロードする
                        $this->uploadFiles(
                            $upload_path,
                            $upload_filenames,
                            array($file),
                            $media_item_values['gmb_media_format'] == "PHOTO"
                        );
                    }
                }
            }

            if ($request->input('delete_media_item2_ids')) {
                // TODO:削除対象のファイルが存在する場合、レコードとS3から削除を行う
                foreach ($request->input('delete_media_item2_ids') as $deleteFileId) {
                    // 対象のレコードを取得する

                    $deleteIds = $arr = explode(',', $deleteFileId);

                    $delMediaItem = MediaItem2::whereIn('media_item2_id', $deleteIds)->first();

                    // 既存の画像がS3に存在する場合は削除する
                    if ($delMediaItem->s3_object_url && Storage::exists($delMediaItem->s3_object_url)) {
                        Storage::delete($delMediaItem->s3_object_url);
                    }
                    if ($delMediaItem->s3_object_url && Storage::exists($this->getThumbnailFilenames($delMediaItem->s3_object_url))) {
                        Storage::delete($this->getThumbnailFilenames($delMediaItem->s3_object_url));
                    }

                    // 下書き状態のみ写真変更が可能なため、削除する場合は対象のレコードを削除する　おそらくwhereInなどで取得後にdeleteするようにするかも
                    MediaItem2::whereIn('media_item2_id', $deleteIds)->delete();
                }
            }


            $uploadPathFiles = $media_item2_group->mediaItems()->pluck('s3_object_url')->unique()->toArray();
            $gmbSourceUrls = $media_item2_group->mediaItems()->pluck('gmb_source_url')->unique()->toArray();

            $media_item2_group->s3_object_url = implode(',', $uploadPathFiles);
            $media_item2_group->gmb_source_url = implode(',', $gmbSourceUrls);
            $media_item2_group->save();

            if($isUpdate) {
                MediaItem2::where('media_item2_group_id', $media_item2_group->media_item2_group_id)
                    ->update(
                        [
                            'gmb_location_association_category' => $filtered_request_array['gmb_location_association_category'],
                            'sync_type' => $filtered_request_array['sync_type'],
                            'sync_status' => $filtered_request_array['sync_status'],
                            'scheduled_sync_time' => $filtered_request_array['scheduled_sync_time'],
                        ]
                    );
            }



            return $media_item2_group;
        } catch (Exception $e) {
            \Log::debug($e);
            foreach ($rollbackPathFiles as $rollback_file) {
                if (Storage::exists($rollback_file)) {
                    Storage::delete($rollback_file);
                }
                if (Storage::exists($this->getThumbnailFilenames($rollback_file))) {
                    Storage::delete($this->getThumbnailFilenames($rollback_file));
                }
            }
            throw $e;
        }
    }

    private function getCondition(Request $request)
    {
        $accounts = $this->getAccounts();
        if ($request->filled('account')) {
            $accounts = $accounts
                ->where('account_id', $request->input('account'));
        }
        $accounts = $accounts
            ->select('account_id')
            ->pluck('account_id')
            ->all();

        $dateRanges = $this->getDateRanges($request);

        $photos = MediaItem2Group::active()
            ->whereIn('media_item2_groups.account_id', $accounts);

        $start_date = $dateRanges['startDate'];
        $end_date = $dateRanges['endDate'] ? Carbon::parse($dateRanges['endDate'])->addDay() : null;
        $category = $request->input('category');
        $sync_status = SyncStatusType::getDescription($request->input('syncStatus'));
        if ($start_date) {
            $photos = $photos->whereHas('mediaItems2', function ($query) use ($request, $start_date) {
                $query->where('media_items2.sync_time', '>=', $start_date);
                if (!$request->filled('startDate')) {
                    $query->orWhereNull('media_items2.sync_time');
                }
            });
        }

        if ($end_date) {
            $photos = $photos->whereHas('mediaItems2', function ($query) use ($end_date) {
                $query->where('media_items2.sync_time', '<', $end_date);
            });
        }

        if ($category) {
            $photos = $photos->where('media_item2_groups.gmb_location_association_category', '=', $category);
        }

        if ($sync_status) {
            $photos = $photos->whereHas('mediaItems2', function($query) use ($sync_status) {
                $query->where('media_items2.sync_status', '=', $sync_status);
            });
        }

        $photos = $photos
            ->orderBy('media_item2_groups.media_item2_group_id', 'desc');

        return $photos;
    }

    private function getAccounts()
    {
        $accounts = Account::getMyAccounts(Auth::id());

        return $accounts;
    }

    private function getDateRanges(Request $request)
    {
        if (session(config('formConst.SESSION_CONTRACT_TYPE'))) {
            $contract_type = session(config('formConst.SESSION_CONTRACT_TYPE'));
        } else {
            $contract_type = Auth::user()->getContractType() ?? 1;
        }
        $contract_type_period = config('const.CONTRACT_TYPE_PERIOD')[$contract_type];
        $periodStartDate = Carbon::parse(Carbon::now()->subMonths($contract_type_period));
        $periodEndDate = Carbon::parse(Carbon::today());
        if ($contract_type_period == 0) {
            if ($request->filled('startDate')) {
                $startDate = $request->input('startDate');
            } else {
                $startDate = null;
            }
            if ($request->filled('endDate')) {
                $endDate = $request->input('endDate');
            } else {
                $endDate = null;
            }
        } else {
            $inputStartDate = Carbon::parse($request->input('startDate'));
            if ($request->filled('startDate') && $inputStartDate->gte($periodStartDate)) {
                $startDate = $request->input('startDate');
            } else {
                $startDate = $periodStartDate->format('Y-m-d');
            }
            $inputEndDate = Carbon::parse($request->input('endDate'));
            if ($request->filled('endDate') && $inputEndDate->lte($periodEndDate)) {
                $endDate = $request->input('endDate');
            } else {
                $endDate = $periodEndDate->format('Y-m-d');
            }
        }

        $dateRanges = array(
            'startDate' => $startDate,
            'endDate'   => $endDate
        );

        return $dateRanges;
    }

    private function setSyncStatusDelete(Request $request, $media_item2_id)
    {
        $sync_type = null;
        $sync_status = null;
        $scheduled_sync_time = null;

        $item = MediaItem2::findOrFail($media_item2_id);

        if ($item->sync_status == SyncStatusType::getDescription(5) && $item->sync_time) {
            $getNextSyncDateTime = getNextSyncDateTime();
            $sync_type = SyncType::getDescription(3);
            $sync_status = SyncStatusType::getDescription(2);
            $scheduled_sync_time = Carbon::parse($getNextSyncDateTime->format('Y-m-d') . ' ' . $getNextSyncDateTime->format('H:i'))->format('Y-m-d H:i:s');
        } else {
            $sync_type = SyncType::getDescription(3);
            if ($item->sync_status == SyncStatusType::getDescription(1)) {
                $sync_status = SyncStatusType::getDescription(1);
            } else {
                $sync_status = SyncStatusType::getDescription(4);
            }
        }

        $request->merge(['is_deleted' => config('const.FLG_ON')]);
        $request->merge(['sync_type' => $sync_type]);
        $request->merge(['sync_status' => $sync_status]);
        $request->merge(['scheduled_sync_time' => $scheduled_sync_time]);
    }

    private function setSyncStatus($request)
    {
        // https://paraworks.backlog.com/view/GMB-8#comment-26612468
        // ・投稿時刻を選択せずに登録した場合は、下書き状態ということでDRAFTを設定する。
        // ・投稿時刻を選択して登録した場合は、編集完了ということでQUEUEDを設定する。
        // 詳細 → https://docs.google.com/spreadsheets/d/1AsH_BVtmOnsxoQYhBaJg93lh8Dvd7J_u4IV9hhO7S_4/edit#gid=0
        $first_local_post
            = $request->local_post_group_id ? LocalPostGroup::getFirstLocalPost($request->local_post_group_id) : null;
        $sync_status = null;
        $scheduled_sync_time = null;
        // APIからみた「新規」か「修正」かをセットする
        $sync_type = $first_local_post
        && LocalPost::find($first_local_post->local_post_id)->gmb_local_post_id ? "PATCH" : "CREATE";

        if ($request->is_scheduled) {
            // 投稿予約 ＝ API連携日時確定
            $sync_status = 'QUEUED';
            $scheduled_sync_time = Carbon::parse($request->scheduled_sync_time
                . ' ' . $request->scheduled_range)->format('Y-m-d H:i:s');
        } else {
            // 下書き
            $sync_status = 'DRAFT';
            $scheduled_sync_time = null;
        }
        $request->merge(['sync_type' => $sync_type]);
        $request->merge(['sync_status' => $sync_status]);
        $request->merge(['scheduled_sync_time' => $scheduled_sync_time]);
    }

    private function getFilteredRequestArray($request): array
    {
        // 投稿日時がチェックされていなかった場合の調整
        if ($request->is_scheduled != 1) {
            $request->scheduled_sync_time = null;
        }

        $filtered_array = array_filter($request->all(), function ($value, $key) {
            // 配列・Object は削除。
            $isArray = is_array($value);
            $isObject = is_object($value);
            // 日付カラムは空文字列で更新しようとするとエラーになるため、空文字の場合は request から削除したい
            $date_fields = [
                'gmb_event_start_time', 'gmb_event_end_time', /*'scheduled_sync_time',*/
                'sync_time'
            ];
            $isBlankDate = in_array($key, $date_fields) && empty($value);
            return !$isArray && !$isObject && !$isBlankDate;
        }, ARRAY_FILTER_USE_BOTH);


        return $filtered_array;
    }

    /**
     * 画像の保存pathを決定する
     * https://paraworks.backlog.com/view/GMB-24#comment-41311481
     *
     */
    private function getUploadPath(array $filtered_request_array)
    {
        return Util::normalizePath(
            'gmb/media2'
            . '/' . (
                        CategoryType::getDescription(CategoryType::CATEGORY_UNSPECIFIED) === $filtered_request_array['gmb_location_association_category']
                        ? 'default' : mb_strtolower($filtered_request_array['gmb_location_association_category'] )
                    )
            . '/' . ($filtered_request_array['account_id'] ?? "no_account_id")
            . '/' . ($filtered_request_array['media_item2_group_id'] ?? "no_media_item2_group_id")
        );
    }

    private function getUploadFilename($upload_file)
    {
        return Util::normalizePath(
            Carbon::now()->format('YmdHis')
            . '_' . $this->getRandomStr(5)
            . '.' . $upload_file->extension()
        );
    }

    private function getUploadFilenames(array $upload_files): array
    {
        $upload_filenames = [];
        foreach ($upload_files as $index => $upload_file) {
            $upload_filenames[$index]
                = $this->getUploadFilename($upload_file);
        }
        return $upload_filenames;
    }

    private function getThumbnailFilenames($fileName): string
    {
        //拡張子の前に文字列を追加
        $pos = strrpos($fileName, '.'); // .が最後に現れる位置
        if ($pos) {
            return (substr($fileName, 0, $pos) . config('const.THUMBNAIL_PREFIX') . substr($fileName, $pos));
        } else {
            return ($fileName . config('const.THUMBNAIL_PREFIX'));
        }
    }

    private function getRandomStr($length)
    {
        return substr(bin2hex(random_bytes($length)), 0, $length);
    }

    /**
     * Undocumented function
     *
     * @param string $upload_path ベースとなるpath
     * @param array $upload_filenames upload_files と対になる、S3上の保存名
     * @param array $upload_files $request->file() の値
     * @return void
     */
    private function uploadFiles(string $upload_path, array $upload_filenames, array $upload_files)
    {
        if ($upload_files) {
            foreach ($upload_files as $index => $upload_file) {
                // 同一ファイルが存在していなければアップロードする
                if (!Storage::exists($upload_path . '/' . $upload_filenames[$index])) {
                    $upload_file->storeAs(
                        $upload_path,
                        $upload_filenames[$index],
                        ['ACL' => 'public-read']
                    );

                    // サムネイル画像の生成
                    if($this->getMediaFormat($upload_filenames[$index]) == "PHOTO") {
                        $thumbnailImage = Image::make($upload_file)
                            ->resize(300, null, function ($constraint) {
                                $constraint->aspectRatio();
                            });

                        // S3のパスへ画像をアップロード
                        Storage::put(
                            $upload_path . '/' . $this->getThumbnailFilenames($upload_filenames[$index]),
                            (string)$thumbnailImage->encode(),
                            ['ACL' => 'public-read']
                        );
                    }
                }
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param string $upload_filename ファイル名
     * @return boolean
     */
    private function getMediaFormat(string $upload_filename)
    {
        $ext = pathinfo($upload_filename, PATHINFO_EXTENSION);
        if ($ext == 'mp4' || $ext == 'mpeg' || $ext == 'mpg' || $ext == 'avi' || $ext == 'mov' || $ext == 'wmv') {
            return "VIDEO";
        }

        return "PHOTO";

    }

}
